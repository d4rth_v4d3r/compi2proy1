
package compi2.proy1.parsing.logic;

import java_cup.runtime.*;
import compi2.parsing.ReportError;
import compi2.runtime.*;
import java.util.ArrayList;

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
parser code
{:
	protected ReportError callbackObject;
	protected Program compiled;
	protected Object rootTable;

	public LogicParser(String filePathToParse, ReportError objectToCallBack)
			throws java.io.IOException {
		super(new LogicScanner(new java.io.FileInputStream(filePathToParse), objectToCallBack));
		this.callbackObject = objectToCallBack;
		compiled = new Program(filePathToParse);
	}

	public LogicParser(String fileName, java.io.InputStream fileToParse,
			ReportError objectToCallBack) {
		super(new LogicScanner(fileToParse, objectToCallBack));
		this.callbackObject = objectToCallBack;
		compiled = new Program(fileName);
	}
	
	public void importObjects(String vars, Object rootTable) {
		this.rootTable = rootTable;
		this.compiled.setVars(vars);
	}

	@Override
	public void unrecovered_syntax_error(Symbol s) throws Exception {
		this.callbackObject.report_unrecovered_syntax_error(s.value.toString(), s.left, s.right);
	}
	
	@Override
	public void syntax_error(Symbol s) {
		this.callbackObject.report_syntax_error(s.value.toString(), s.left, s.right);
	}
	
	@Override
	public void done_parsing(){
		super.done_parsing();
		this.callbackObject.done_parsing();
		System.out.println(this.compiled.toString());
		this.compiled.configure(64);
		this.compiled.execute("main", false);
		System.out.println(this.compiled.getCode());
		System.out.println(this.compiled.report());
	}
	
	protected Symbol curr_Token() {
		return super.cur_token;
	}

:}

action code
{:
	int temps = 0, etqs = 0, stack = 0;
	Tabla ts = new Tabla(null);
	Tabla actual = ts;

	ArrayList<String> instr = new ArrayList<String>();
	ArrayList<Integer> calls = new ArrayList<Integer>();
	ArrayList<CSymbol> lista = new ArrayList<CSymbol>();
	Tabla syms = new Tabla(null);
	
	CSymbol buscar(Tabla raiz, String id) {
		Tabla aux = raiz;
		while(aux != null) {
			CSymbol s = buscarSimbolo(aux, id);
			if(s != null)
				return s;
			else
				aux = aux.padre;
		}
		
		return null;
	}
	
	CSymbol buscarSimbolo(Tabla raiz, String id) {
		for(CSymbol s: raiz)
			if(id.equals(s.cad))
				return s;
		return null;
	}

	String generaTMP() {
		return "#" + temps++;
	}

	String generaETQ() {
		return String.valueOf(instr.size());
	}

	Integer write(String instr) {
		this.instr.add(instr);
		return this.instr.size() - 1;
	}

	String getVar(Tabla raiz, String id) {
		Tabla aux = raiz;
		int offset = 0;
		while (aux != null) {
			String result = get(aux, id, offset);
			if (result != null)
				return result;
			offset = aux.size();
			aux = aux.padre;
		}

		return "<var:" + id + ">";
	}

	String get(Tabla raiz, String id, int offset) {
		int index = 0;
		for (CSymbol s : raiz)
			if (s.cad.equals(id))
				if (raiz.padre != null) {
					String tmp1 = generaTMP();
					String tmp2 = generaTMP();
					offset += index;
					write("~0 = ~0 + " + offset);
					write(tmp1 + " = ~0");
					write(tmp2 + " = @" + tmp1);
					write("~0 = ~0 - " + offset);
					return tmp2;
				} else
					return "~" + index;
			else
				index++;

		return null;
	}

	String setVar(Tabla raiz, String id) {
		Tabla aux = raiz;
		int offset = 0;
		while (aux != null) {
			String result = set(aux, id, offset);
			if (result != null)
				return result;
			offset = aux.size();
			aux = aux.padre;
		}

		return "<var:" + id + ">";
	}

	String set(Tabla raiz, String id, int offset) {
		int index = 0;
		for (CSymbol s : raiz)
			if (s.cad.equals(id))
				if (raiz.padre != null) {
					String tmp1 = generaTMP();
					offset += index;
					write("~0 = ~0 + " + offset);
					write(tmp1 + " = ~0");
					write("~0 = ~0 - " + offset);
					return "@" + tmp1;
				} else
					return "~" + index;
			else
				index++;

		return null;
	}
	
	String setParam(int offset) {
		String tmp1 = generaTMP();
		write("~0 = ~0 + " + offset);
		write(tmp1 + " = ~0");
		return "@" + tmp1;
	}

	class Tabla extends ArrayList<CSymbol> {
		/**
		 * 
		 */
		private static final long serialVersionUID = -4406768795215256499L;
		Tabla padre = null;

		Tabla(Tabla padre) {
			this.padre = padre;
		}

		@Override
		public String toString() {
			String toString = "-------------------------\n";
			for (CSymbol s : this)
				toString += s.cad + "\n";
			toString += "-------------------------\n";
			return toString;
		}
	}

	class CSymbol {
		String cad, aux = "";
		ArrayList<Integer> etqV = new ArrayList<Integer>(),
				etqF = new ArrayList<Integer>();
		int size = 0;
		TIPO tipo = TIPO.NULL;
		ACCESO acceso = ACCESO.NULL;

		CSymbol() {
			cad = "?";
		}

		CSymbol(String cad) {
			this.cad = cad;
		}

		CSymbol(String cad, TIPO tipo, ACCESO acceso) {
			this.cad = cad;
			this.tipo = tipo;
			this.acceso = acceso;
		}
		
		@Override
		public String toString() {
			return String.format("-------------------\ncad = %s;\nsize = %d;\ntipo = %s;\nacceso = %s;\naux = %s;"
				, cad, size, tipo.toString(), acceso.toString(), aux);
		}
	}

	enum TIPO {
		LOGICO, CADENA, ENTERO, DECIMAL, NULL;
	}

	enum ACCESO {
		OCULTO, VISIBLE, PROTEGIDO, NULL;
	}
	
	int buscarFuncion(String id) {
		int index = 0;
		for(CSymbol s : syms)
			if(s.cad.equals(id))
				return index;
			else
				++index;
		return -1;
	}	

:}

terminal Object cadena, entero, decimal, logico, incluir, oculto, visible, protegido, to, asign, aumentar, disminuir, mientras, hacer, para, si, sino, no, vida, poder, regeneracion, movimiento, rotar, mover,
escudo, recargar, disparar, horario, antihorario, arriba, abajo, izquierda, derecha, opbracket, clbracket, lparen, rparen, opbrace, clbrace, comma, colon, semicolon, equal, plus, minus, times, div,
mod, sqr, pow, not, and, or, less, greater, gequals, lequals, equals, nequals, id, hasta, detener, regresar, uminus, verdadero, falso, valcadena, valentero, valdecimal;

non terminal CSymbol S, MAT_FILE, LINCLUDE, INCLUDE, CODE, PROC, FUNCTION, METHOD, ACCESS, FDECL, FBODY, TYPE, LPARAMS, PARAM, INSTR, LINSTR, IF, WHILE, FOR, DO_WHILE, ASIGN, DECL, EXPR,
GFUNCTION, CALLF, COUNT, IF_, ELSE, ELIF, LVAL, VAL, BOOL, REL;

precedence right or;
precedence right and;
precedence right not;
precedence right greater, less, gequals, lequals, equals, nequals;
precedence right plus, minus;
precedence right times, div;
precedence right pow, sqr;
precedence right uminus;
precedence right mod;
precedence left id;
precedence left lparen;
precedence left rparen;

S			::=		MAT_FILE {: int index = 0;
								for(CSymbol var : ts) {
								 	instr.add("~" + index + " = " 
								 		+ (var.tipo == TIPO.ENTERO ? "0" : var.tipo == TIPO.DECIMAL ? "0.00" : var.tipo == TIPO.CADENA ? "\"abc\"" : "0"));
								 	index++;
								 }
							parser.compiled.addFunction("__init__", instr, 0); :}
			;
							
MAT_FILE	::=		LINCLUDE {: parser.compiled.addVar("p"); actual.add(new CSymbol("p")); :}  CODE
			|		{: parser.compiled.addVar("p"); actual.add(new CSymbol("p")); :} CODE
			;
							
LINCLUDE	::=		LINCLUDE INCLUDE
			|		INCLUDE
			;
							
INCLUDE		::=		incluir lparen valcadena rparen
			;
							
CODE		::=		CODE PROC
			|		PROC
			;
							
PROC		::=		FUNCTION
			|		METHOD
			|		DECL {: for(CSymbol s : lista) {s.acceso = ACCESO.OCULTO; parser.compiled.addVar(s.cad); actual.add(s);} lista.clear(); :}
			|		ACCESS:_a DECL	{: for(CSymbol s : lista) {s.acceso = _a.acceso; parser.compiled.addVar(s.cad); actual.add(s);} lista.clear(); :}
			;			
							
ACCESS		::=		oculto {: RESULT = new CSymbol(); RESULT.acceso = ACCESO.OCULTO; :}
			|		visible {: RESULT = new CSymbol(); RESULT.acceso = ACCESO.VISIBLE; :}
			|		protegido {: RESULT = new CSymbol(); RESULT.acceso = ACCESO.PROTEGIDO; :}
			;
							
TYPE		::=		cadena {: RESULT = new CSymbol(); RESULT.tipo = TIPO.CADENA; RESULT.aux = RESULT.tipo.toString(); :}
			|		entero {: RESULT = new CSymbol(); RESULT.tipo = TIPO.ENTERO; RESULT.aux = RESULT.tipo.toString(); :}
			|		decimal {: RESULT = new CSymbol(); RESULT.tipo = TIPO.DECIMAL; RESULT.aux = RESULT.tipo.toString(); :}
			|		logico	{: RESULT = new CSymbol(); RESULT.tipo = TIPO.LOGICO; RESULT.aux = RESULT.tipo.toString(); :}
			;
							
FUNCTION	::=		FDECL:_f colon TYPE:_t {: _f.size = lista.size(); _f.tipo = _t.tipo; actual.add(_f); for(CSymbol s : lista) actual.add(s); lista.clear(); :} FBODY	
						{:  for(Integer i : calls){instr.set(i, instr.get(i).replaceAll("真", "" + stack));} parser.compiled.addFunction(_f.cad, instr, temps); syms.add(_f); stack = 0; calls.clear(); temps = 0; instr.clear(); :}
			|		FDECL:_f {: _f.size = lista.size(); for(CSymbol s : lista) actual.add(s); lista.clear(); :} FBODY 
						{:  for(Integer i : calls){instr.set(i, instr.get(i).replaceAll("真", "" + stack));} parser.compiled.addFunction(_f.cad, instr, temps); syms.add(_f); stack = 0; calls.clear(); temps = 0; instr.clear(); :}
			;							
							
FDECL		::=		ACCESS:_a id:_i opbracket LPARAMS:_l clbracket {: RESULT = new CSymbol(_i.toString(), TIPO.NULL, _a.acceso); RESULT.aux = _l.aux; actual = new Tabla(actual); :}
			|		ACCESS:_a id:_i opbracket clbracket  {: RESULT = new CSymbol(_i.toString(), TIPO.NULL, _a.acceso); actual = new Tabla(actual); :}
			|		id:_i opbracket LPARAMS:_l clbracket 	{: RESULT = new CSymbol(_i.toString(), TIPO.NULL, ACCESO.OCULTO); RESULT.aux = _l.aux; actual = new Tabla(actual); :}
			|		id:_i opbracket clbracket {: RESULT = new CSymbol(_i.toString(), TIPO.NULL, ACCESO.OCULTO); actual = new Tabla(actual); :}
			;
							
LPARAMS		::=		LPARAMS:_l comma PARAM:_p {: RESULT = _l; RESULT.aux += "_" + _p.aux; :}	
			|		PARAM:_p	{: RESULT = _p; :}
			;
							
PARAM		::=		id:_i TYPE:_t {: RESULT = new CSymbol(_i.toString(), _t.tipo, ACCESO.OCULTO); lista.add(RESULT); RESULT.aux = RESULT.tipo.toString(); :}
			;
							
FBODY		::=		opbrace LINSTR clbrace:c {: stack = actual.size(); actual = actual.padre; :}
			;
							
LINSTR		::=		LINSTR INSTR
			|		INSTR
			;			
							
INSTR		::=		CALLF semicolon {: calls.add(write("~0 = ~0 - 真")); :}
			|		DECL:_d {: for(CSymbol s : lista) actual.add(s); lista.clear(); :}
			|		ASIGN 
			|		WHILE
			|		FOR
			|		DO_WHILE
			|		IF_
			|		GFUNCTION
			;
							
DECL		::=		id:_i comma DECL:_d  {: lista.add(new CSymbol(_i.toString(), _d.tipo, ACCESO.NULL)); :}
			|		id:_i  to TYPE:_t semicolon	 {: lista.add(new CSymbol(_i.toString(), _t.tipo, ACCESO.NULL)); :}
			;
							
ASIGN		::=		EXPR:_e asign id:_i semicolon {: RESULT = new CSymbol(); write(String.format("%s = %s", setVar(actual, _i.toString()), _e.cad)); :}
			;
							
CALLF		::=		id:_i lparen LVAL:_l rparen	{: write("~0 = ~0 - " + _l.size); write("&" + buscarFuncion(_i.toString())+"()"); CSymbol m = buscar(syms, _i.toString());
						if(m == null)
							parser.callbackObject.report_semantic_error("El metodo " + _i + " no existe", _ileft, _iright);
						else
							if(!_l.aux.equals(m.aux))
							parser.callbackObject.report_semantic_error("Los parametros del metodo no son correctos. Se esperaba: (" + 
								m.aux.replaceAll("_", ",") + ")." + _l.aux, _ileft, _iright); 
							else
								for(Integer i : _l.etqV){instr.set(i, instr.get(i).replaceAll("真", "" + (m.tipo == TIPO.NULL ? "0" : "1")));}:}																						
			|		id:_i lparen rparen {: write("&" + buscarFuncion(_i.toString())+"()"); CSymbol m = buscar(syms, _i.toString()); :}
			;
						
LVAL		::=		LVAL:_e comma EXPR:_e2	{: write(setParam(1) + " = " + _e2.cad);  RESULT = _e; RESULT.aux = RESULT.tipo.toString() + "_" + _e2.tipo.toString(); RESULT.size++; :}
			|		EXPR:_e {: calls.add(write("~0 = ~0 + 真")); RESULT = _e; RESULT.aux = RESULT.tipo.toString(); 
						String tmp1 = generaTMP();
						RESULT.etqV.add(write("~0 = ~0 + 真"));
						write(tmp1 + " = ~0");
						write("@" + tmp1 + " = " + _e.cad);
						RESULT.size++;:}
			;
			
WHILE		::=		mientras {: RESULT = new CSymbol(generaETQ()); :} lparen BOOL:_b rparen {: for(Integer i : _b.etqV) instr.set(i, instr.get(i).replaceAll("\\?\\?", generaETQ()));  actual = new Tabla(actual); :} 
						FBODY  {: write("goto " + RESULT.cad); for(Integer i : _b.etqF) instr.set(i, instr.get(i).replaceAll("真", generaETQ())); :}
			;
							
DO_WHILE	::=		hacer  {: RESULT = new CSymbol(generaETQ()); actual = new Tabla(actual); :} 
						FBODY mientras 
							lparen BOOL:_b {: for(Integer i : _b.etqV) instr.set(i, instr.get(i).replaceAll("\\?\\?", RESULT.cad)); for(Integer i : _b.etqF) instr.set(i, instr.get(i).replaceAll("真", generaETQ())); :} rparen semicolon
			;							
							
FOR			::=		para lparen id:_i equal EXPR:_e1 {: write(getVar(actual, _i.toString()) + " = " + _e1.cad); :} 
						hasta EXPR:_e2 rparen  {: RESULT = new CSymbol(); RESULT.cad = generaETQ(); RESULT.etqV.add(write(String.format("if %s 真 %s then goto ??", getVar(actual, _i.toString()), _e2.cad))); 
							RESULT.etqF.add(write("goto 真")); actual = new Tabla(actual); for(Integer i : RESULT.etqV) instr.set(i, instr.get(i).replaceAll("\\?\\?", generaETQ())); :} FBODY 
								COUNT:_c {: if(_c.aux == null) for(Integer i : RESULT.etqV) instr.set(i, instr.get(i).replaceAll("真", "<="));
									else for(Integer i : RESULT.etqV) instr.set(i, instr.get(i).replaceAll("真", ">=")); 
										write(getVar(actual, _i.toString()) + " = " + getVar(actual, _i.toString()) + _c.cad); write("goto " + RESULT.cad); 
											for(Integer i : RESULT.etqF) instr.set(i, instr.get(i).replaceAll("真", generaETQ()));:}
			;
							
COUNT		::=		aumentar semicolon {: RESULT = new CSymbol(" + 1"); RESULT.aux = null; :}
			|		disminuir semicolon {: RESULT = new CSymbol(" - 1"); RESULT.aux = "�"; :}
			;		
							
IF_			::=		IF:_b ELSE:_e
			|		IF:_b
			;

IF			::=		si lparen BOOL:_b rparen {: for(Integer i : _b.etqV) instr.set(i, instr.get(i).replaceAll("\\?\\?", generaETQ())); actual = new Tabla(actual); :} FBODY {: for(Integer i : _b.etqF) instr.set(i, instr.get(i).replaceAll("真", generaETQ())); :}
			;
							
ELSE		::=		ELIF:_e1 ELSE:_e2
			|		no {: actual = new Tabla(actual); :} FBODY
			;
							
ELIF		::=		sino lparen BOOL:_b rparen {: for(Integer i : _b.etqV) instr.set(i, instr.get(i).replaceAll("\\?\\?", generaETQ()));  actual = new Tabla(actual); :} FBODY {: for(Integer i : _b.etqF) instr.set(i, instr.get(i).replaceAll("真", generaETQ())); :}
			;							
							
EXPR		::=		plus EXPR:_e1 EXPR:_e2 {: RESULT = new CSymbol(); RESULT.cad = generaTMP(); write(String.format("%s = %s + %s", RESULT.cad,  _e1.cad, _e2.cad)); :}
			|		minus EXPR:_e1 EXPR:_e2 {: RESULT = new CSymbol(); RESULT.cad = generaTMP(); write(String.format("%s = %s - %s", RESULT.cad, _e1.cad, _e2.cad)); :}
			|		times EXPR:_e1 EXPR:_e2	{: RESULT = new CSymbol(); RESULT.cad = generaTMP(); write(String.format("%s = %s * %s", RESULT.cad, _e1.cad, _e2.cad)); :}
			|		div EXPR:_e1 EXPR:_e2	{: RESULT = new CSymbol(); RESULT.cad = generaTMP(); write(String.format("%s = %s / %s", RESULT.cad, _e1.cad, _e2.cad)); :}
			|		pow EXPR:_e1 EXPR:_e2	{: RESULT = new CSymbol(); RESULT.cad = generaTMP(); write(String.format("%s = %s ^ %s", RESULT.cad, _e1.cad, _e2.cad)); :}
			|		sqr EXPR:_e	{: RESULT = new CSymbol(); RESULT.cad = generaTMP(); write(String.format("%s = -/ %s", RESULT.cad, _e.cad)); :}
			|		mod EXPR:_e1 EXPR:_e2	{: RESULT = new CSymbol(); RESULT.cad = generaTMP(); write(String.format("%s = %s % %s", RESULT.cad, _e1.cad, _e2.cad)); :}
			|		minus EXPR:_e	{: RESULT = new CSymbol(); RESULT.cad = generaTMP(); write(String.format("%s = - %s", RESULT.cad, _e.cad)); :}
					%prec uminus
			|		lparen EXPR:_e rparen {: RESULT = new CSymbol(); RESULT.cad = _e.cad; :}
			|		id:_i {: RESULT = new CSymbol(getVar(actual, _i.toString())); CSymbol var = buscar(actual, _i.toString()); 
						RESULT.tipo = var == null? RESULT.tipo : var.tipo; 
							if(var == null)
								parser.callbackObject.report_syntax_error("La variable " + _i + " no existe.", _ileft, _iright);:}
			|		VAL:_v {: RESULT =  _v; :}															
			|		CALLF:_c {: RESULT = _c; calls.add(write("~0 = ~0 + 真")); :}													
			;
							
BOOL		::=		and BOOL:_r1 {: for(Integer i : _r1.etqV) instr.set(i, instr.get(i).replaceAll("\\?\\?", generaETQ())); :} BOOL:_r2 {: _r1.etqF.addAll(_r2.etqF); _r1.etqV =_r2.etqV; RESULT =_r1;  :}
			|		or BOOL:_r1  {: for(Integer i : _r1.etqF) instr.set(i, instr.get(i).replaceAll("真", generaETQ())); :} BOOL:_r2 {: _r1.etqF = _r2.etqF; _r1.etqV.addAll(_r2.etqV); RESULT =_r1;  :}
			|		not BOOL:_r {: RESULT = _r; ArrayList<Integer> aux = _r.etqF; _r.etqF = _r.etqV; _r.etqV = aux; :}
			| 		lparen BOOL:_r rparen {: RESULT = _r; :}
			|		REL:_r	{: RESULT = _r; :}
			|		EXPR:_r {: RESULT = _r; RESULT.etqV.add(write(String.format("if %s then goto ??", _r.cad))); RESULT.etqF.add(write("goto 真")); :}
			|		verdadero {: RESULT = new CSymbol(); RESULT.etqV.add(write(String.format("if 1 then goto ??"))); RESULT.etqF.add(write("goto 真")); :}
			|		falso {: RESULT = new CSymbol(); RESULT.etqV.add(write(String.format("if 0 then goto ??"))); RESULT.etqF.add(write("goto 真")); :}
			;
							
REL			::=		greater EXPR:_e1 EXPR:_e2 {: RESULT = new CSymbol(); RESULT.etqV.add(write(String.format("if %s > %s then goto ??", _e1.cad, _e2.cad))); RESULT.etqF.add(write("goto 真")); :}
			|		gequals EXPR:_e1 EXPR:_e2 {: RESULT = new CSymbol(); RESULT.etqV.add(write(String.format("if %s >= %s then goto ??", _e1.cad, _e2.cad))); RESULT.etqF.add(write("goto 真")); :}
			|		less EXPR:_e1 EXPR:_e2 {: RESULT = new CSymbol(); RESULT.etqV.add(write(String.format("if %s < %s then goto ??", _e1.cad, _e2.cad))); RESULT.etqF.add(write("goto 真")); :}
			|		lequals EXPR:_e1 EXPR:_e2 {: RESULT = new CSymbol(); RESULT.etqV.add(write(String.format("if %s <= %s then goto ??", _e1.cad, _e2.cad))); RESULT.etqF.add(write("goto 真")); :}
			|		equals EXPR:_e1 EXPR:_e2 {: RESULT = new CSymbol(); RESULT.etqV.add(write(String.format("if %s == %s then goto ??", _e1.cad, _e2.cad))); RESULT.etqF.add(write("goto 真")); :}
			|		nequals EXPR:_e1 EXPR:_e2 {: RESULT = new CSymbol(); RESULT.etqV.add(write(String.format("if %s != %s then goto ??", _e1.cad, _e2.cad))); RESULT.etqF.add(write("goto 真")); :}
			;
												
VAL			::=		valcadena:_v {: RESULT = new CSymbol(_v.toString(), TIPO.CADENA, ACCESO.NULL); :}								
			|		valentero:_v {: RESULT = new CSymbol(_v.toString(), TIPO.ENTERO, ACCESO.NULL); :}									
			|		valdecimal:_v {: RESULT = new CSymbol(_v.toString(), TIPO.DECIMAL, ACCESO.NULL); :}
			;							
