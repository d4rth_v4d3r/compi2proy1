
package compi2.proy1.parsing.Robot;

import java_cup.runtime.*;
import compi2.parsing.Logger;
import compi2.runtime.*;
import java.util.ArrayList;

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
parser code
{:
	protected Logger callbackObject;
	protected boolean[][] esc;
	protected Escenario es;

	public EscenarioParser(String filePathToParse, Logger objectToCallBack)
			throws java.io.IOException {
		super(new EscenarioScanner(new java.io.FileInputStream(filePathToParse),
				objectToCallBack));
		this.callbackObject = objectToCallBack;
		this.callbackObject.fileLogger = filePathToParse;
		esc = null;
		es = new Escenario();
	}

	public EscenarioParser(String fileName, java.io.InputStream fileToParse,
			Logger objectToCallBack) {
		super(new EscenarioScanner(fileToParse, objectToCallBack));
		esc = null;
		es = new Escenario();
		this.callbackObject = objectToCallBack;
		this.callbackObject.fileLogger = fileName;
	}

	@Override
	public void unrecovered_syntax_error(Symbol s) throws Exception {
		this.callbackObject.report_unrecovered_syntax_error(s.value.toString(),
				s.left, s.right);
	}

	@Override
	public void syntax_error(Symbol s) {
		this.callbackObject.report_syntax_error(
				"Token no esperado: " + s.value.toString(), s.left, s.right);
	}

	@Override
	public void done_parsing() {
		super.done_parsing();
		this.callbackObject.finishParsing(es);
	}

	protected Symbol curr_Token() {
		return super.cur_token;
	}
:}

action code
{:
	int rows = 1;
	int columns = 1;
	boolean flag = false;
	
	class Point {
		int x = 0, y = 0;
		
		Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
:}

terminal mayor, menor, nombre, igual, coma, lbracket, rbracket, guion, objetos, obj, fila, columna, tipo, dimension, descripcion, escenario, codigo,
div;
terminal String LID, string;
terminal Boolean bool;
terminal Integer num;

non terminal S, ESCENARIO, AETQ_ESC, CETQ_ESC, ATRS_ESC, ATR_ESC, AETQ_NOMBRE, CETQ_NOMBRE, AETQ_DESC, CETQ_DESC, AETQ_DIM, CETQ_DIM, ATRS_DIM,
LOBJS, OBJ, CETQ_OBJ, ETQ_COLUMNA, ETQ_FILA, AETQ_OBJS, CETQ_OBJS;

non terminal ArrayList<Point> VAL, FILA, COLUMNA, AETQ_OBJ;

S			::=		ESCENARIO
			;
			
ESCENARIO	::=		AETQ_ESC ATRS_ESC CETQ_ESC
			|		error:e {: parser.callbackObject.report_syntax_error("Error en la etiqueta la estructura de escenario.", eleft, eright); :}
			;
			
AETQ_ESC	::=		menor escenario codigo igual num:n mayor {: parser.es.setId(n); :}
			;
			
CETQ_ESC	::=		menor div escenario mayor
			;
			
ATRS_ESC	::=		ATRS_ESC ATR_ESC
			|		ATR_ESC
			;
			
ATR_ESC		::=		AETQ_NOMBRE LID:l CETQ_NOMBRE   {: parser.es.setNombre(l); :}
			|		AETQ_DESC LID:l CETQ_DESC		{: parser.es.setDescripcion(l); :}
			|		AETQ_DIM ATRS_DIM CETQ_DIM 		{: parser.esc = new boolean[rows][columns]; parser.es.initCells(rows, columns); :}
			|		AETQ_OBJS LOBJS CETQ_OBJS		{: :}
			|		error:e {: parser.callbackObject.report_syntax_error("Error en el atributo de escenario.", eleft, eright); :}
			;
			
AETQ_NOMBRE	::=		menor nombre mayor
			;
			
CETQ_NOMBRE	::=		menor div nombre mayor
			;
			
AETQ_DESC	::=		menor descripcion mayor
			;
			
CETQ_DESC	::=		menor div descripcion mayor
			;
			
AETQ_DIM	::=		menor dimension mayor
			;			
			
ATRS_DIM	::=		ETQ_FILA ETQ_COLUMNA
			|		ETQ_COLUMNA ETQ_FILA
			;
			
ETQ_FILA	::=		menor fila mayor num:n menor div fila mayor {: rows = n; :}
			;
			
ETQ_COLUMNA	::=		menor columna mayor num:n menor div columna mayor {: columns = n; :}
			;
			
CETQ_DIM	::=		menor div dimension mayor
			;
			
			
AETQ_OBJS	::=		menor objetos mayor
			;
			
CETQ_OBJS	::=		menor div objetos mayor
			;
			
LOBJS		::=		LOBJS OBJ
			|		OBJ
			;
			
OBJ			::=		AETQ_OBJ:a bool:b CETQ_OBJ {: for(Point p : a) { 
						if(p.x < parser.es.columnas() && p.x >= 0 && p.y < parser.es.filas() && p.y >= 0) 
							{ parser.esc[p.y][p.x] = b; parser.es.setObstaculo(b, p.x, p.y); } } :}
			|		error:e {: parser.callbackObject.report_syntax_error("Error en el nodo de objeto.", eleft, eright); :}
			;
			
AETQ_OBJ	::=		menor obj FILA:f COLUMNA:c tipo igual string:s mayor {: RESULT = new ArrayList<Point>(); 
																			for(Point p1 : f)
																				for(Point p2 : c)
																					if(p1.x == p2.x && p1.y == p2.y) {
																						RESULT.add(p2);
																						parser.es.setTipo(s, p1.x, p1.y);
																					}
																		:}
			|		menor obj COLUMNA:f FILA:c tipo igual string:s mayor {: RESULT = new ArrayList<Point>();
																			for(Point p1 : f)
																				for(Point p2 : c)
																					if(p1.x == p2.x && p1.y == p2.y) {
																						RESULT.add(p2); 
																						parser.es.setTipo(s, p1.x, p1.y);
																					}
																		:}
			|		menor obj FILA:f tipo igual string:s mayor {: RESULT = f; for(Point p1 : f) parser.es.setTipo(s, p1.x, p1.y); :}
			|		menor obj COLUMNA:c tipo igual string:s mayor {: RESULT = c; for(Point p1 : c) parser.es.setTipo(s, p1.x, p1.y); :}
			;
			
CETQ_OBJ	::=		menor div obj mayor
			;
			
FILA		::=		fila {: flag = false; :} igual VAL:v {: RESULT = v; :}
			;
			
COLUMNA		::=		columna {: flag = true; :} igual VAL:v {: RESULT = v; :}
			;
			
VAL			::=		num:n {: RESULT = new ArrayList<Point>();
							if(flag)
								for(int i = 0; i < rows; ++i) {
									RESULT.add(new Point(n, i));
								}
							else 
								for(int i = 0; i < columns; ++i) {
									RESULT.add(new Point(i, n));
								} :}
			|		lbracket num:n1 coma num:n2 rbracket 
					{: 		RESULT = new ArrayList<Point>();
							if(flag)
								for(int i = 0; i < rows; ++i) {
									RESULT.add(new Point(n1, i));
									RESULT.add(new Point(n2, i));
								}
							else 
								for(int i = 0; i < columns; ++i) {
									RESULT.add(new Point(i, n1));
									RESULT.add(new Point(i, n2));
								}
					 :}
			|		lbracket num:n1 guion num:n2 rbracket
					{: 	RESULT = new ArrayList<Point>();
						if(n1 > n2) parser.callbackObject.report_semantic_error("El valor de inicio es mayor que el de fin.", n2left, n2right);
						else {
							if(flag)
								for(int j = n1; j <= n2; ++j) {
									for(int i = 0; i < rows; ++i)
										RESULT.add(new Point(j, i));
								}
							else 
								for(int j = n1; j <= n2; ++j) {
									for(int i = 0; i < columns; ++i)
										RESULT.add(new Point(i, j));
								}
							} :}
			;		
