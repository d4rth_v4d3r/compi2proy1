
package compi2.proy1.parsing.Robot;

import java_cup.runtime.*;
import java.io.IOException;
import compi2.parsing.Logger;
import compi2.runtime.*;

%%

%class RoboScanner

%init{
		
%init}

%eofval{	
		return sym(RoboParsingSyms.EOF);
%eofval}

%public
%cup
%unicode
%ignorecase
%line  	//definimos el uso de contador de lineas
%char  	//definimos el uso de contador de caracteres
%column

%{
	Logger callbackObject = null;
	boolean flag = false;
	
	RoboScanner(java.io.InputStream is, Logger callbackObject) {
		this(is);
		this.callbackObject = callbackObject;
	}
	
	private Symbol sym(int type)
	{
		return sym(type, yytext());
	}

	private Symbol sym(int type, Object value)
	{
		return new Symbol(type, yyline+1, yycolumn+1, value);
	}

%}

NUM 		= 	[0-9]+
string		=	\"[^\"]*\"

%state ANY, ANYTOK

%%
<YYINITIAL>{NUM}				{	return sym(RoboParsingSyms.num, new Integer(yytext()));	}
<YYINITIAL>{string}				{	return sym(RoboParsingSyms.string, new String(yytext().substring(1, yytext().length()-1)));	}
<ANY>">"						{	yybegin(ANYTOK); return sym(RoboParsingSyms.mayor);	}
<YYINITIAL>"<"					{	flag = true; return sym(RoboParsingSyms.menor);	}
<YYINITIAL>">"					{	return sym(RoboParsingSyms.mayor);	}
<YYINITIAL>"/"					{	flag = false; return sym(RoboParsingSyms.div);	}
<YYINITIAL>"codigo"				{	return sym(RoboParsingSyms.codigo);	}
<YYINITIAL>"="					{	return sym(RoboParsingSyms.igual);	}
<YYINITIAL>"nombre"				{	if(flag) yybegin(ANY); return sym(RoboParsingSyms.nombre);	}
<YYINITIAL>"movimiento"			{	return sym(RoboParsingSyms.movimiento);	}
<YYINITIAL>"regeneracion"		{	return sym(RoboParsingSyms.regeneracion);	}
<YYINITIAL>"escudo"				{	return sym(RoboParsingSyms.escudo);	}
<YYINITIAL>"inteligencia"		{	return sym(RoboParsingSyms.inteligencia);	}
<YYINITIAL>"metodo"				{	return sym(RoboParsingSyms.metodo);	}
<YYINITIAL>"origen"				{	return sym(RoboParsingSyms.origen);	}
<YYINITIAL>"configuracion"		{	return sym(RoboParsingSyms.configuracion);	}
<YYINITIAL>"default"			{	return sym(RoboParsingSyms.default_);	}
<YYINITIAL>"vida"				{	return sym(RoboParsingSyms.vida);	}
<YYINITIAL>"poder"				{	return sym(RoboParsingSyms.poder);	}
<YYINITIAL>"robot"				{	return sym(RoboParsingSyms.robot);	}
<ANYTOK>[^"<"]+					{	yybegin(YYINITIAL); return sym(RoboParsingSyms.LID, yytext().trim()); }
<ANY, YYINITIAL>[\n\r\f]+		{   }
<ANY, YYINITIAL>[" "\t]+		{	}

<YYINITIAL>.					{	this.callbackObject.report_lexical_error("El token no es valido " + yytext(), yyline+1, yycolumn); }