
package compi2.proy1.parsing.logic;

import java_cup.runtime.*;
import compi2.parsing.Logger;

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
%%
%{
	Logger callbackObject = null;
	
	LogicScanner(java.io.InputStream is, Logger callbackObject) {
		this(is);
		this.callbackObject = callbackObject;
	}
	
	private void report_lexical_error() {
		try {
			this.callbackObject.report_lexical_error(yytext(), yyline+1, yycolumn+1);
		} catch(NullPointerException e) {
			System.err.println((yyline+1) + "|" + (yycolumn+1) + " 'Callback Object is null: Taking system err as output log. Lexical error " + yytext() + "'");
		}
	}
	
	int yyline(){
		return this.yyline;
	}
	
	int yychar() {
		return this.yychar;
	}

	private Symbol sym(int type) {
		return sym(type, yytext());
	}

	private Symbol sym(int type, Object value) {
		return new Symbol(type, yyline + 1, yycolumn+1, value);
	}
%}

%init{
		
%init}

%eofval{	
		return sym(LogicParsingSyms.EOF);
%eofval}

%class LogicScanner
%public
%cup
%unicode
%ignorecase
%line  	//definimos el uso de contador de lineas
%char  	//definimos el uso de contador de caracteres
%column
%state COMMENT1, COMMENT2

character	= 	[A-Za-z]
digit		=	[0-9]
cadena		=	\"[^\"]*\"
entero		=	{digit}+
decimal		= {entero}"."{entero}
id 			=	{character}({character}|{digit}|"_")*

%%

//=================================== INICIO DE EXPRESIONES REGULARES  =======================================

//------------------- SECCION DE TIPOS -------------------// 
<YYINITIAL>		{entero}	{ return sym(LogicParsingSyms.valentero, new Integer(yytext()));	}
<YYINITIAL>		{cadena}	{ return sym(LogicParsingSyms.valcadena, new String(yytext()));	}
<YYINITIAL>		{decimal}	{ return sym(LogicParsingSyms.valdecimal, new Float(yytext()));  }
<YYINITIAL>		"true"		{ return sym(LogicParsingSyms.verdadero, new Boolean(yytext())); 	}
<YYINITIAL>		"false"		{ return sym(LogicParsingSyms.falso, new Boolean(yytext())); 	}
<YYINITIAL>		"cadena"	{ return sym(LogicParsingSyms.cadena); 	}
<YYINITIAL>		"decimal"	{ return sym(LogicParsingSyms.decimal); 	}
<YYINITIAL>		"entero"	{ return sym(LogicParsingSyms.entero); 	}
<YYINITIAL>		"logico"	{ return sym(LogicParsingSyms.logico); 	}

//------------------- SECCION DE KEYWORDS -------------------// 
<YYINITIAL>		"incluir"	{ return sym(LogicParsingSyms.incluir); 	}
<YYINITIAL>		"oculto"	{ return sym(LogicParsingSyms.oculto); 	}
<YYINITIAL>		"visible"	{ return sym(LogicParsingSyms.visible); 	}
<YYINITIAL>		"protegido"	{ return sym(LogicParsingSyms.protegido); 	}
<YYINITIAL>		"to"		{ return sym(LogicParsingSyms.to); 	}
<YYINITIAL>		"asig"		{ return sym(LogicParsingSyms.asign); 	}
<YYINITIAL>		"aumentar"	{ return sym(LogicParsingSyms.aumentar); 	}
<YYINITIAL>		"disminuir"	{ return sym(LogicParsingSyms.disminuir); 	}
<YYINITIAL>		"mientras"	{ return sym(LogicParsingSyms.mientras); 	}
<YYINITIAL>		"hacer"		{ return sym(LogicParsingSyms.hacer); 	}
<YYINITIAL>		"para"		{ return sym(LogicParsingSyms.para); 	}
<YYINITIAL>		"hasta"		{ return sym(LogicParsingSyms.hasta); 	}
<YYINITIAL>		"si"		{ return sym(LogicParsingSyms.si); 	}
<YYINITIAL>		"sino"		{ return sym(LogicParsingSyms.sino); 	}
<YYINITIAL>		"no"		{ return sym(LogicParsingSyms.no); 	}
<YYINITIAL>		"break"		{ return sym(LogicParsingSyms.detener); 	}

//------------------- SECCION DE ROBO KEYWORDS -------------------// 
<YYINITIAL>		"vida"		{ return sym(LogicParsingSyms.vida); 	}
<YYINITIAL>		"poder"		{ return sym(LogicParsingSyms.poder); 	}
<YYINITIAL>	"regeneracion"	{ return sym(LogicParsingSyms.regeneracion); }
<YYINITIAL>	"movimiento"	{ return sym(LogicParsingSyms.movimiento); }
<YYINITIAL>		"rotar"		{ return sym(LogicParsingSyms.rotar); 	}
<YYINITIAL>		"mover"		{ return sym(LogicParsingSyms.mover); 	}
<YYINITIAL>		"escudo"	{ return sym(LogicParsingSyms.escudo); 	}
<YYINITIAL>		"recargar"	{ return sym(LogicParsingSyms.recargar); 	}
<YYINITIAL>		"disparar"	{ return sym(LogicParsingSyms.disparar); 	}
<YYINITIAL>		"horario"	{ return sym(LogicParsingSyms.horario); 	}
<YYINITIAL>	 "antihorario"	{ return sym(LogicParsingSyms.antihorario); 	}
<YYINITIAL>		"arriba"	{ return sym(LogicParsingSyms.arriba); 	}
<YYINITIAL>		"abajo"		{ return sym(LogicParsingSyms.abajo); 	}
<YYINITIAL>		"izquierda"	{ return sym(LogicParsingSyms.izquierda); 	}
<YYINITIAL>		"derecha"	{ return sym(LogicParsingSyms.derecha); 	}

//------------------- SECCION DE SIMBOLOS DE METODOS, CICLOS E INSTRUCCIONES -------------------// 
<YYINITIAL>		"["			{ return sym(LogicParsingSyms.opbracket); 	}
<YYINITIAL>		"]"			{ return sym(LogicParsingSyms.clbracket); 	}
<YYINITIAL>		"("			{ return sym(LogicParsingSyms.lparen); 	}
<YYINITIAL>		")"			{ return sym(LogicParsingSyms.rparen); 	}
<YYINITIAL>		"{"			{ return sym(LogicParsingSyms.opbrace); 	}
<YYINITIAL>		"}"			{ return sym(LogicParsingSyms.clbrace); 	}
<YYINITIAL>		","			{ return sym(LogicParsingSyms.comma); 	}
<YYINITIAL>		":"			{ return sym(LogicParsingSyms.colon); 	}
<YYINITIAL>		";"			{ return sym(LogicParsingSyms.semicolon); 	}
<YYINITIAL>		"="			{ return sym(LogicParsingSyms.equal); 	}

//------------------- SECCION DE SIMBOLOS ARITMETICOS -------------------// 
<YYINITIAL>		"+"			{ return sym(LogicParsingSyms.plus); 	}
<YYINITIAL>		"-"			{ return sym(LogicParsingSyms.minus); 	}
<YYINITIAL>		"*"			{ return sym(LogicParsingSyms.times); 	}
<YYINITIAL>		"/"			{ return sym(LogicParsingSyms.div); 	}
<YYINITIAL>		"%"			{ return sym(LogicParsingSyms.mod); 	}
<YYINITIAL>		"sqr"		{ return sym(LogicParsingSyms.sqr); 	}
<YYINITIAL>		"pow"		{ return sym(LogicParsingSyms.pow); 	}

//------------------- SECCION DE SIMBOLOS BOOLEANOS Y RELACIONALES -------------------// 
<YYINITIAL>		"not"		{ return sym(LogicParsingSyms.not); 	}
<YYINITIAL>		"and"		{ return sym(LogicParsingSyms.and); 	}
<YYINITIAL>		"or"		{ return sym(LogicParsingSyms.or); 	}
<YYINITIAL>		">"			{ return sym(LogicParsingSyms.greater); 	}
<YYINITIAL>		"<"			{ return sym(LogicParsingSyms.less); 	}
<YYINITIAL>		">="		{ return sym(LogicParsingSyms.gequals); 	}
<YYINITIAL>		"<="		{ return sym(LogicParsingSyms.lequals); 	}
<YYINITIAL>		"=="		{ return sym(LogicParsingSyms.equals); 	}
<YYINITIAL>		"!="		{ return sym(LogicParsingSyms.nequals); 	}

//------------------- SECCION DE MISCELANEOS -------------------// 
<YYINITIAL>		{id}		{	return sym(LogicParsingSyms.id); }
<YYINITIAL>		[" "\t]+	{	}
<YYINITIAL>		"//"		{	yybegin(COMMENT1); }
<YYINITIAL>		"/*"		{	yybegin(COMMENT2); }
		[\n\r\f]+			{  if(yystate() == COMMENT1) yybegin(YYINITIAL); }
<COMMENT2>	"*/"			{	yybegin(YYINITIAL); }

<YYINITIAL>		.			{ 	report_lexical_error(); }
<COMMENT1, COMMENT2>	.	{ 	}