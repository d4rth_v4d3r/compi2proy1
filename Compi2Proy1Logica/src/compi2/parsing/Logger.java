/**
 * 
 */
package compi2.parsing;

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
public abstract class Logger {
	public String fileLogger = "";
	
	/**
	 * This is a callback that a parser do when a lexical error occurs.
	 * 
	 * @param message
	 *            The error message.
	 * @param line
	 *            The line where the error is.
	 * @param column
	 *            The column where the error is.
	 */
	public abstract void report_lexical_error(String message, int line, int column);

	/**
	 * This is a callback that a parser do when a syntax error occurs.
	 * 
	 * @param message
	 *            The error message.
	 * @param line
	 *            The line where the error is.
	 * @param column
	 *            The column where the error is.
	 */
	public abstract void report_syntax_error(String message, int line, int column);

	/**
	 * This is a callback that a parser do when a semantic error occurs.
	 * 
	 * @param message
	 *            The error message.
	 * @param line
	 *            The line where the error is.
	 * @param column
	 *            The column where the error is.
	 */
	public abstract void report_semantic_error(String message, int line, int column);

	/**
	 * This is a callback that a parser do when it finishes to parse.
	 */
	public abstract void done_parsing();

	/**
	 * This is a callback that a parser do when it can not recover from a syntax
	 * error occurs.
	 * 
	 * @param message
	 *            The error message.
	 * @param line
	 *            The line where the error is.
	 * @param column
	 *            The column where the error is.
	 */
	public abstract void report_unrecovered_syntax_error(String message, int line,
			int column);

	/**
	 * This is a callback that a parser do when a fatal(implementation) error
	 * occurs.
	 */
	public abstract void internal_error(String message);

	/**
	 * Ejecutar este metodo para devolver el resultado del parsing;
	 * 
	 * @param RESULT
	 */
	public abstract void finishParsing(Object RESULT);
}
