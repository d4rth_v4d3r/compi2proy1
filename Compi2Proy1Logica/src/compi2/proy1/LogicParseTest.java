/**
 * 
 */
package compi2.proy1;

import compi2.parsing.Logger;
import compi2.proy1.parsing.logic.LogicParser;
import compi2.runtime.Escenario;
import compi2.runtime.Game;
import compi2.runtime.GameEventLogger;
import compi2.runtime.Program;
import compi2.runtime.Robot;
import compi2.runtime.SerializationHandler;

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
public class LogicParseTest extends Logger {
	static Game a;
	static Escenario esc;

	public LogicParseTest() {
		GameEventLogger gl = new GameEventLogger() {

			@Override
			public void rotarRobot(Robot r, int angle) {
				// TODO Auto-generated method stub
				/*
				 * System.out.println("El robot " + r.getNombre() + " esta en: "
				 * + angle);
				 */
				System.out.println("Rotacion:\n");
				System.out.println(a.gameStatus(-1, -1));
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void recargar(Robot r) {
				// TODO Auto-generated method stub
				/*
				 * System.out.println("El robot " + r.getNombre() +
				 * " ha ganado vida: " + r.getVida());
				 */
				System.out.println("Recarga:\n");
				System.out.println(a.gameStatus(-1, -1));
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public Program obtenerArchivoLogica(String path) {
				// TODO Auto-generated method stub
				return (Program) SerializationHandler.cargarPrograma(path);
			}

			@Override
			public void moverRobot(Robot r, int x, int y) {
				// TODO Auto-generated method stub
				/*
				 * System.out.println("El robot " + r.getNombre() +
				 * " se movio a: " + x + "," + y);
				 */
				System.out.println("Mover:\n");
				System.out.println(a.gameStatus(-1, -1));
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void dispararRobot(Robot r, int x, int y) {
				// TODO Auto-generated method stub
				/*
				 * System.out.println("El robot " + r.getNombre() +
				 * " hizo un disparo en: " + x + "," + y);
				 */
				System.out.println("Disparar " + x + "," + y + ":\n");
				System.out.println(a.gameStatus(x, y));
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void escudoRobot(Robot r, int n) {
				// TODO Auto-generated method stub
				System.out.println("Escudo:\n");
				System.out.println(a.gameStatus(-1, -1));
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void gameFinished(Robot winner) {
				// TODO Auto-generated method stub
				System.out.println(winner.getNombre()
						+ " ha ganado la batalla :)");
			}

			@Override
			public void empate() {
				// TODO Auto-generated method stub
				System.out.println("Se ha llegado a un empate B)");
			}
		};

		Robot r1 = new Robot();
		r1.setNombre("R2");
		r1.setAngle(0);
		r1.setEscudo(0);
		r1.setIa_metodo("main");
		r1.setIa_origen(System.getProperty("user.dir")
				+ System.getProperty("file.separator") + "test.x");
		r1.setMovimiento(4);
		r1.setPoder(100);
		r1.setRegeneracion(10);
		r1.setVida(100);
		r1.setX(0);
		r1.setY(0);

		Robot r2 = new Robot();
		r2.setNombre("D2");
		r2.setAngle(0);
		r2.setEscudo(0);
		r2.setIa_metodo("main");
		r2.setIa_origen(System.getProperty("user.dir")
				+ System.getProperty("file.separator") + "test.x");
		r2.setMovimiento(4);
		r2.setPoder(100);
		r2.setRegeneracion(10);
		r2.setVida(100);
		r2.setX(3);
		r2.setY(3);

		a = new Game(gl, null, r1, r2);
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		LogicParser parser = new LogicParser("ia.alg", LogicParseTest.class
				.getClassLoader().getResourceAsStream(
						"resources/TestFiles/ia.alg"), new LogicParseTest());
		parser.parse();
		a.execute();
	}

	@Override
	public void report_lexical_error(String message, int line, int column) {
		// TODO Auto-generated method stub
		System.out.printf("%s| Lexico: %s. Linea %d y columna %d\n",
				this.fileLogger, message, line, column);
	}

	@Override
	public void report_syntax_error(String message, int line, int column) {
		// TODO Auto-generated method stub
		System.out.printf("%s| Sintactico: %s. Linea %d y columna %d\n",
				this.fileLogger, message, line, column);
	}

	@Override
	public void report_semantic_error(String message, int line, int column) {
		// TODO Auto-generated method stub
		System.out.printf("%s| Semantico: %s. Linea %d y columna %d\n",
				this.fileLogger, message, line, column);
	}

	@Override
	public void done_parsing() {
		// TODO Auto-generated method stub
		System.out.printf("Ha finalizado el analisis.\n");
	}

	@Override
	public void report_unrecovered_syntax_error(String message, int line,
			int column) {
		// TODO Auto-generated method stub
		System.out
				.printf("Error irrecuperable de sintaxis en el simbolo '%s'. Linea %d y columna %d\n",
						message, line, column);
	}

	@Override
	public void internal_error(String message) {
		// TODO Auto-generated method stub
		System.out.printf("Error interno: \n", message);
	}

	@Override
	public void finishParsing(Object RESULT) {
		// TODO Auto-generated method stub
		SerializationHandler.guardarPrograma(System.getProperty("user.dir")
				+ System.getProperty("file.separator") + "test.x", RESULT);
	}

}
