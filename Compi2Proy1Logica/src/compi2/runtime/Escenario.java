/**
 * 
 */
package compi2.runtime;

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
public class Escenario {

	public class Celda {
		public boolean obstaculo;
		public String tipo;
	}

	private Celda[][] obstaculo;
	private String nombre;
	private int id;
	private String descripcion;

	public Escenario() {
		initCells(1, 1);
	}
	
	public void init(int filas, int columnas) {
		this.obstaculo = new Celda[filas][columnas];
	}

	public void initCells(int filas, int columnas) {
		this.obstaculo = new Celda[filas][columnas];
		for (int i = 0; i < filas; i++) {
			for (int j = 0; j < columnas; j++) {
				this.obstaculo[i][j] = new Celda();
				this.obstaculo[i][j].tipo = "_default_";
			}
		}
	}

	public boolean isObstaculo(int x, int y) {
		if (x >= columnas() || x < 0 || y >= filas() || y < 0) {
			System.err.println("Fuera de los limites: ( " + x + " , " + y
					+ " )");
			return true;
		}
		return obstaculo[y][x].obstaculo;
	}

	public void setObstaculo(boolean obstaculo, int x, int y) {
		if (x >= columnas() || x < 0 || y >= filas() || y < 0) {
			System.err.println("Fuera de los limites: ( " + x + " , " + y
					+ " )");
			return;
		}
		this.obstaculo[y][x].obstaculo = obstaculo;
	}

	public String getTipo(int x, int y) {
		if (x >= columnas() || x < 0 || y >= filas() || y < 0) {
			System.err.println("Fuera de los limites: ( " + x + " , " + y
					+ " )");
			return "_default_";
		}
		return obstaculo[y][x].tipo;
	}

	public void setTipo(String tipo, int x, int y) {
		if (x >= columnas() || x < 0 || y >= filas() || y < 0) {
			System.err.println("Fuera de los limites: ( " + x + " , " + y
					+ " )");
			return;
		}
		this.obstaculo[y][x].tipo = tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int filas() {
		return this.obstaculo.length;
	}

	public int columnas() {
		return this.obstaculo[0].length;
	}

	public void setCelda(int x, int y, Celda c) {
		this.obstaculo[x][y] = c;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer s = new StringBuffer();

		for (int i = 0; i < filas(); i++) {
			for (int j = 0; j < columnas(); j++) {
				s.append(obstaculo[i][j].tipo + "." + obstaculo[i][j].obstaculo
						+ "\t");
			}
			s.append("\n");
		}

		return s.toString();
	}

}
