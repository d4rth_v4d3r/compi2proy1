/**
 * 
 */
package compi2.runtime;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
public class Game {
	private Escenario escenario;
	private GameEventLogger logger;
	private Robot r1, r2;
	private Program pr1, pr2;
	private int turno1, turno2;
	private Thread hilo1, hilo2;

	private class Point {
		int x = 0, y = 0;

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return x + "," + y;
		}
	}

	public Program getPr1() {
		return pr1;
	}

	public void setPr1(Program pr1) {
		this.pr1 = pr1;
	}

	public Program getPr2() {
		return pr2;
	}

	public void setPr2(Program pr2) {
		this.pr2 = pr2;
	}

	public Game(GameEventLogger logger, Escenario escenario, Robot r1, Robot r2) {
		this.escenario = escenario;
		this.logger = logger;
		this.r1 = r1;
		this.r2 = r2;
		this.pr1 = logger.obtenerArchivoLogica(r1.getIa_origen());
		this.pr2 = logger.obtenerArchivoLogica(r2.getIa_origen());
		this.turno1 = r1.getMovimiento();
		this.turno2 = r2.getMovimiento();
	}

	public void execute() {
		initProgram();
		this.hilo1 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				pr1.execute(r1.getIa_metodo(), false);
			}
		});

		this.hilo2 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				pr2.updateMovimiento(0);
				pr2.execute(r2.getIa_metodo(), false);
			}
		});

		Timer timeout = new Timer(72000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Aquí el código que queramos ejecutar.
				pr1.destroy = true;
				pr2.destroy = true;
				logger.empate();
			}
		});
		timeout.setRepeats(false);
		timeout.start();

		hilo1.start();
	}

	public void initProgram() {
		this.pr1.configure(64, r1, this, turno1);
		this.pr2.configure(64, r2, this, turno2);
		this.r1.setX(0);
		this.r1.setY(0);
		this.r2.setX(escenario.columnas() - 1);
		this.r2.setY(escenario.filas() - 1);
		this.r1.setAngle(0);
		this.r2.setAngle(0);
	}

	public void terminarTurno(Program context) {
		Program nuevoTurno = null;
		if (context == pr1) {
			if (!hilo2.isAlive())
				hilo2.start();
			nuevoTurno = pr2;
			nuevoTurno.updateMovimiento(turno2);
			r2.setMovimiento(turno2);
		}
		if (context == pr2) {
			nuevoTurno = pr1;
			nuevoTurno.updateMovimiento(turno1);
			r1.setMovimiento(turno1);
		}
	}

	public void doMove(Program context, Robot robo) {
		robo.setMovimiento(robo.getMovimiento() - 1);
		context.updateMovimiento(robo.getMovimiento());
		if (robo.getMovimiento() == 0) {
			terminarTurno(context);
			while (robo.getMovimiento() == 0 && !context.destroy)
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	/**
	 * Mueve el robot en la direccion especificada (ARRIBA, ABAJO, IZQUIERDA,
	 * DERECHA), un numero finito de pasos. Si el robot no se puede desplazar,
	 * esta rutina lo gira hasta que lo pueda hacer.
	 * 
	 * @param robo
	 *            El robot a mover
	 * @param dir
	 *            La direccion (ARRIBA, ABAJO, IZQUIERDA, DERECHA)
	 * @param n
	 *            La cantidad de pasos a dar en la direccion 'dir'
	 */
	public void mover(Program context, Robot robo, int dir, int n) {
		if (n > 0)
			for (int i = 0; i < n; i++) {
				if (context.destroy)
					return;
				Point p = getPoint(robo, dir);
				int x = p.x + robo.getX();
				int y = p.y + robo.getY();

				if (y >= 0 && y < escenario.filas() && x >= 0
						&& x < escenario.columnas()
						&& !escenario.isObstaculo(x, y)) {
					escenario.setObstaculo(false, robo.getX(), robo.getY());
					robo.setX(x);
					robo.setY(y);
					escenario.setObstaculo(true, robo.getX(), robo.getY());
					logger.moverRobot(robo, x, y);
					doMove(context, robo);
				} else {
					System.err
							.println("El robot se atasco :( buscando una salida...");
					girar(context, robo, 90, 1);
				}
			}
		else
			System.err
					.println("La cantidad de pasos debe ser un numero entero positivo --por tanto el robot no se movio--");

	}

	/**
	 * Gira el robot en un angulo de 90° o -90°, n veces.
	 * 
	 * @param robo
	 *            El robot a girar
	 * @param angle
	 *            El angulo de giro (+- 90°)
	 * @param n
	 *            El numero de giros a realizar
	 */
	public void girar(Program context, Robot robo, int angle, int n) {
		if (n > 0)
			for (int i = 0; i < n; i++) {
				if (context.destroy)
					return;
				int a = robo.getAngle() + angle;
				robo.setAngle((a < 0 ? a * -1 : a) % 360);
				logger.rotarRobot(robo, robo.getAngle());
				doMove(context, robo);
			}
		else
			System.err
					.println("La cantidad de pasos debe ser un numero entero positivo --por tanto el robot no giro--");
	}

	/**
	 * Recarga al robot con un porcentaje de vida
	 * 
	 * @param robo
	 *            El robot que recupera vidas
	 */
	public void recargar(Program context, Robot robo) {
		if (context.destroy)
			return;
		robo.setVida(robo.getVida() + robo.getVida() * robo.getRegeneracion()
				/ 100);
		logger.recargar(robo);
		doMove(context, robo);
	}

	public void disparar(Program context, Robot rx, int n) {
		if (context.destroy)
			return;
		Robot a = null, b = null;
		if (rx == r1) {
			a = r1;
			b = r2;
		} else if (rx == r2) {
			a = r2;
			b = r1;
		} else {
			System.err
					.println("El robot no corresponde a este juego --por tanto no dispara--");
			return;
		}

		Point p = getPoint(a, 0); // Disparo arriba

		int x = a.getX() + p.x;
		int y = a.getY() + p.y;

		if (b.getX() == x && b.getY() == y) {
			b.setVida((b.getVida() - a.getPoder()) + b.getEscudo());
			context.updateVida(b.getVida());
			logger.dispararRobot(a, x, y);
			if (b.getVida() <= 0) {
				pr1.destroy = true;
				pr2.destroy = true;
				logger.gameFinished(a);
			}
		}

		else if (y >= 0 && y < escenario.filas() && x >= 0
				&& x < escenario.columnas() && !escenario.isObstaculo(x, y))
			logger.dispararRobot(a, x, y);
		doMove(context, a);
	}

	public void escudo(Program context, Robot robo, int n) {
		if (context.destroy)
			return;
		robo.setEscudo(n);
		doMove(context, robo);
	}

	private Point getPoint(Robot robo, int dir) {
		Point p = new Point();

		switch (robo.getAngle()) {
		case 0:
			switch (dir) {
			case 0: // arriba
				p.x = -1;
				p.y = 0;
				return p; // abajo
			case 1:
				p.x = 1;
				p.y = 0;
				return p; // izquierda
			case 2:
				p.x = 0;
				p.y = 1;
				return p; // derecha
			case 3:
				p.x = 0;
				p.y = -1;
				return p;
			default:
				return p;
			}
		case 90:
			switch (dir) {
			case 0: // arriba
				p.x = 0;
				p.y = 1;
				return p; // abajo
			case 1:
				p.x = 0;
				p.y = -1;
				return p; // izquierda
			case 2:
				p.x = 1;
				p.y = 0;
				return p; // derecha
			case 3:
				p.x = -1;
				p.y = 0;
				return p;
			default:
				return p;
			}
		case 180:
			switch (dir) {
			case 0: // arriba
				p.x = 1;
				p.y = 0;
				return p; // abajo
			case 1:
				p.x = -1;
				p.y = 0;
				return p; // izquierda
			case 2:
				p.x = 0;
				p.y = -1;
				return p; // derecha
			case 3:
				p.x = 0;
				p.y = 1;
				return p;
			default:
				return p;
			}

		case 270:
			switch (dir) {
			case 0: // arriba
				p.x = 0;
				p.y = -1;
				return p; // abajo
			case 1:
				p.x = 0;
				p.y = 1;
				return p; // izquierda
			case 2:
				p.x = -1;
				p.y = 0;
				return p; // derecha
			case 3:
				p.x = 1;
				p.y = 0;
				return p;
			default:
				return p;
			}
		default:
			return p;
		}
	}

	public String gameStatus(int x, int y) {
		// TODO Auto-generated method stub
		for (int i = 0; i < escenario.filas(); i++) {
			for (int j = 0; j < escenario.columnas(); j++) {
				System.out.printf("|");
				if (x == j && y == i)
					System.out.print("☼");
				if (r1.getX() == j && r1.getY() == i)
					switch (r1.getAngle()) {
					case 0:
						System.out.print("←1");
						break;
					case 90:
						System.out.print("↓1");
						break;
					case 180:
						System.out.print("→1");
						break;
					case 270:
						System.out.print("↑1");
						break;
					default:
						break;
					}
				else if (r2.getX() == j && r2.getY() == i)
					switch (r2.getAngle()) {
					case 0:
						System.out.print("←2");
						break;
					case 90:
						System.out.print("↓2");
						break;
					case 180:
						System.out.print("→2");
						break;
					case 270:
						System.out.print("↑2");
						break;
					default:
						break;
					}
				else if (escenario.isObstaculo(j, i))
					System.out.print("▓");

				System.out.print("\t");
			}
			System.out.println();
		}

		System.out.println(r1.toString());
		System.out.println(r2.toString());
		return "";
	}
}
