/**
 * 
 */
package compi2.runtime;

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
public interface GameEventLogger {

	/**
	 * Este callback se dispara cuando un robot se mueve en un programa.
	 * 
	 * @param r
	 *            El robot que se mueve
	 * @param x
	 *            La coordenada x a la que se mueve
	 * @param y
	 *            La coordenada y a la que se mueve
	 */
	public void moverRobot(Robot r, int x, int y);

	/**
	 * Este callback se dispara cuando un robot dispara en un programa.
	 * 
	 * @param r
	 *            El robot que dispara
	 * @param x
	 *            La coordenada x en la que cae el disparo
	 * @param y
	 *            La coordenada y en la que cae el disparo
	 */
	public void dispararRobot(Robot r, int x, int y);

	/**
	 * El robot actualiza su escudo
	 * 
	 * @param r
	 *            El robot que actualiza su escudo.
	 * @param n
	 *            La cantidad del nuevo nivel de escudo
	 */
	public void escudoRobot(Robot r, int n);

	/**
	 * Este callback se dispara cuando un robot gira en el programa.
	 * 
	 * @param r
	 *            El robot que rota.
	 * @param angle
	 *            El angulo de giro actual
	 */
	public void rotarRobot(Robot r, int angle);

	/**
	 * Este callback se genera cuando un robot recarga vida en un programa
	 * 
	 * @param r
	 *            El robot que recarga
	 */
	public void recargar(Robot r);

	/**
	 * Obtiene el programa que esta en la direccion especificada
	 * 
	 * @param path
	 *            La direccion a buscar
	 * @return El programa si lo puede levantar, sino null.
	 */
	public Program obtenerArchivoLogica(String path);

	/**
	 * Esta llamada se ejecuta cuando el juego termina.
	 * 
	 * @param winner
	 *            El robot vencedor.
	 */
	public void gameFinished(Robot winner);

	/**
	 * Este callback es para manejar los empates.
	 */
	public void empate();
}
