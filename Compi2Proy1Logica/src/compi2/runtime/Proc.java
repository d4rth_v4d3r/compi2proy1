package compi2.runtime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;

public class Proc implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5179925559390368452L;
	private Object[] temp;
	public Object[] stack;
	public Object[] var;
	private String[] code;
	private String id;

	public Proc(String id, int temp_size, ArrayList<String> code) {
		this.id = id;
		if (temp_size > 0)
			setTemp(temp_size);
		if (code != null)
			setCode(code, code.size());
	}

	public void setTemp(int temps) {
		this.temp = new Object[temps];
	}

	public Object[] getTemp() {
		return this.temp;
	}

	public void setCode(Iterable<String> code, int lines) {
		this.code = new String[lines];
		int i = 0;
		for (String c : code) {
			this.code[i] = c;
			i++;
		}
	}

	public void editInstruction(int line, String newInstr) {
		this.code[line] = newInstr;
	}

	public String[] getCode() {
		return this.code;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return this.id;
	}

	public Object calcExpr(Matcher currLine) {
		String x = currLine.group(1);
		Object y = null;
		String dir = currLine.group(2);
		Object z = null;
		if (dir == null) {
			y = loadValue(currLine.group(4));
		} else {
			y = loadValue(dir);
			z = loadValue(currLine.group(4));
		}
		String op = currLine.group(3);

		if (op == null)
			if (x.charAt(0) == '#') // Variable Temporal
				return temp[Integer.valueOf(x.substring(1))] = y;

			else if (x.charAt(0) == '~') // Variable Global
				return var[Integer.valueOf(x.substring(1))] = y;

		if (op.equals("+"))
			if (x.charAt(0) == '#') // Variable Temporal
				if (y instanceof Float || z instanceof Float)
					return temp[Integer.valueOf(x.substring(1))] = ((Number) y)
							.floatValue() + ((Number) z).floatValue();
				else
					return temp[Integer.valueOf(x.substring(1))] = ((Integer) y)
							+ ((Integer) z);
			else if (x.charAt(0) == '~') // Variable Global
				if (y instanceof Float || z instanceof Float)
					return var[Integer.valueOf(x.substring(1))] = ((Number) y)
							.floatValue() + ((Number) z).floatValue();
				else
					return var[Integer.valueOf(x.substring(1))] = ((Integer) y)
							+ ((Integer) z);

		if (op.equals("-") && z != null)
			if (x.charAt(0) == '#') // Variable Temporal
				if (y instanceof Float || z instanceof Float)
					return temp[Integer.valueOf(x.substring(1))] = ((Number) y)
							.floatValue() - ((Number) z).floatValue();
				else
					return temp[Integer.valueOf(x.substring(1))] = ((Integer) y)
							- ((Integer) z);
			else if (x.charAt(0) == '~') // Variable Global
				if (y instanceof Float || z instanceof Float)
					return var[Integer.valueOf(x.substring(1))] = ((Number) y)
							.floatValue() - ((Number) z).floatValue();
				else
					return var[Integer.valueOf(x.substring(1))] = ((Integer) y)
							- ((Integer) z);

		if (op.equals("*"))
			if (x.charAt(0) == '#') // Variable Temporal
				if (y instanceof Float || z instanceof Float)
					return temp[Integer.valueOf(x.substring(1))] = ((Number) y)
							.floatValue() * ((Number) z).floatValue();
				else
					return temp[Integer.valueOf(x.substring(1))] = ((Integer) y)
							* ((Integer) z);
			else if (x.charAt(0) == '~') // Variable Global
				if (y instanceof Float || z instanceof Float)
					return var[Integer.valueOf(x.substring(1))] = ((Number) y)
							.floatValue() * ((Number) z).floatValue();
				else
					return var[Integer.valueOf(x.substring(1))] = ((Integer) y)
							* ((Integer) z);

		if (op.equals("/") && z != null)
			if (x.charAt(0) == '#') // Variable Temporal
				if (y instanceof Float || z instanceof Float)
					return temp[Integer.valueOf(x.substring(1))] = ((Number) y)
							.floatValue() / ((Number) z).floatValue();
				else
					return temp[Integer.valueOf(x.substring(1))] = ((Integer) y)
							/ ((Integer) z);
			else if (x.charAt(0) == '~') // Variable Global
				if (y instanceof Float || z instanceof Float)
					return var[Integer.valueOf(x.substring(1))] = ((Number) y)
							.floatValue() / ((Number) z).floatValue();
				else
					return var[Integer.valueOf(x.substring(1))] = ((Integer) y)
							/ ((Integer) z);

		if (op.equals("%"))
			if (x.charAt(0) == '#') // Variable Temporal
				if (y instanceof Float || z instanceof Float)
					return temp[Integer.valueOf(x.substring(1))] = ((Number) y)
							.floatValue() % ((Number) z).floatValue();
				else
					return temp[Integer.valueOf(x.substring(1))] = ((Integer) y)
							% ((Integer) z);
			else if (x.charAt(0) == '~') // Variable Global
				if (y instanceof Float || z instanceof Float)
					return var[Integer.valueOf(x.substring(1))] = ((Number) y)
							.floatValue() % ((Number) z).floatValue();
				else
					return var[Integer.valueOf(x.substring(1))] = ((Integer) y)
							% ((Integer) z);

		if (op.equals("^"))
			if (x.charAt(0) == '#') // Variable Temporal
				if (y instanceof Float || z instanceof Float)
					return temp[Integer.valueOf(x.substring(1))] = Math.pow(
							((Number) y).floatValue(),
							((Number) z).floatValue());
				else
					return temp[Integer.valueOf(x.substring(1))] = (int) Math
							.pow(((Integer) y), ((Integer) z));
			else if (x.charAt(0) == '~') // Variable Global
				if (y instanceof Float || z instanceof Float)
					return var[Integer.valueOf(x.substring(1))] = Math.pow(
							((Number) y).floatValue(),
							((Number) z).floatValue());
				else
					return var[Integer.valueOf(x.substring(1))] = (int) Math
							.pow(((Integer) y), ((Integer) z));

		if (op.equals("-/"))
			if (x.charAt(0) == '#') // Variable Temporal
				if (y instanceof Float)
					return temp[Integer.valueOf(x.substring(1))] = Math
							.sqrt(((Number) y).floatValue());
				else
					return temp[Integer.valueOf(x.substring(1))] = (int) Math
							.sqrt(((Number) y).intValue());
			else if (x.charAt(0) == '~') // Variable Global
				if (y instanceof Float)
					return var[Integer.valueOf(x.substring(1))] = Math
							.sqrt(((Number) y).floatValue());
				else
					return var[Integer.valueOf(x.substring(1))] = (int) Math
							.sqrt(((Number) y).intValue());

		if (op.equals("-")) {
			if (x.charAt(0) == '#') // Variable Temporal
				if (y instanceof Float || z instanceof Float)
					return temp[Integer.valueOf(x.substring(1))] = -1
							* ((Number) y).floatValue();
				else
					return temp[Integer.valueOf(x.substring(1))] = -1
							* ((Integer) y);
			else if (x.charAt(0) == '~') // Variable Global
				if (y instanceof Float || z instanceof Float)
					return var[Integer.valueOf(x.substring(1))] = -1
							* ((Number) y).floatValue();
				else
					return var[Integer.valueOf(x.substring(1))] = -1
							* ((Integer) y);
		}

		return null;
	}

	public Object inStack(Matcher currLine) {
		String x = currLine.group(1);
		String z = currLine.group(2);

		Object y = null;

		if (x.charAt(0) == '@') {
			y = loadValue(x.substring(1));
			return stack[(Integer) y] = loadValue(z);
		} else if (z.charAt(0) == '@') {
			y = loadValue(z.substring(1));

			if (x.charAt(0) == '#') // Variable Temporal
				return temp[Integer.valueOf(x.substring(1))] = stack[(Integer) y];

			else if (x.charAt(0) == '~') // Variable Global
				return var[Integer.valueOf(x.substring(1))] = stack[(Integer) y];
		}

		return null;
	}

	public Object checkExpr(Matcher currLine) {
		Object x = loadValue(currLine.group(1));
		Integer z = null;
		String op = "";
		Object y = null;

		op = currLine.group(2);
		if (op != null)
			y = loadValue(currLine.group(3));
		String dir = currLine.group(4);
		if (dir != null)
			z = Integer.valueOf(dir);
		else
			z = Integer.valueOf(currLine.group(2));

		if (op == null || op.equals(""))
			if (x instanceof Number && ((Number) x).floatValue() != 0)
				return z;
			else
				return null;

		if (op.equals("=="))
			if (x instanceof Number && y instanceof Number)
				return ((Number) x).floatValue() == ((Number) y).floatValue() ? z
						: null;
			else
				return x.toString().equals(y.toString()) ? z : null;

		if (op.equals("!="))
			if (x instanceof Number && y instanceof Number)
				return ((Number) x).floatValue() != ((Number) y).floatValue() ? z
						: null;
			else
				return !x.toString().equals(y.toString()) ? z : null;

		if (op.equals("<="))
			if (x instanceof Number && y instanceof Number)
				return ((Number) x).floatValue() <= ((Number) y).floatValue() ? z
						: null;
			else
				return null;

		if (op.equals(">="))
			if (x instanceof Number && y instanceof Number)
				return ((Number) x).floatValue() >= ((Number) y).floatValue() ? z
						: null;
			else
				return null;

		if (op.equals("<"))
			if (x instanceof Number && y instanceof Number)
				return ((Number) x).floatValue() < ((Number) y).floatValue() ? z
						: null;
			else
				return null;

		if (op.equals(">"))
			if (x instanceof Number && y instanceof Number)
				return ((Number) x).floatValue() > ((Number) y).floatValue() ? z
						: null;
			else
				return null;

		return null;
	}

	public Object jump(Matcher currLine) {
		return Integer.parseInt(currLine.group(1));
	}

	public Object callFunction(Matcher currLine) {
		return Integer.valueOf(currLine.group(1));
	}

	private Object loadValue(String dir) {
		if (dir.charAt(0) == '#')
			return temp[Integer.valueOf(dir.substring(1))];
		else if (dir.charAt(0) == '~')
			return var[Integer.valueOf(dir.substring(1))];
		else
			try {
				return Integer.valueOf(dir);
			} catch (Exception i) {
				try {
					return Float.valueOf(dir);
				} catch (Exception f) {
					return dir;
				}
			}
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return id + "@temps = " + Arrays.toString(temp);
	}

}
