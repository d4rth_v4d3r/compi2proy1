/**
 * 
 */
package compi2.runtime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author julian
 * 
 */
public class Program implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8394090998614309688L;
	private String name;
	private String globalVars;
	private Object[] stack, var;
	private ArrayList<Proc> procs;
	private Pattern p_if, p_expr, p_goto, p_stack, p_call, p_mover, p_rotar,
			p_disparar, p_recargar, p_escudo;
	private Robot robo;
	public Game logger;
	public boolean destroy = false;

	// P = 0; VIDA = 1; PODER = 2; REGENERACION = 3; MOVIMIENTO = 4;

	private static final String exprRegex = "([#~]\\d+)\\s*=\\s*([#~]\\d+|\\d+|\\d+\\.\\d+|\\\".*\\\")?\\s+(\\+|-|\\*|/|\\^|-/|%)?\\s*([#~]\\d+|\\d+|\\d+\\.\\d+|\\\".*\\\")",
			ifRegex = "if\\s+([#~]\\d+|\\d+|\\d+\\.\\d+|\\\".+\\\")\\s*(==|!=|<|>|<=|>=)?\\s*([#~]\\d+|\\d+|\\d+\\.\\d+|\\\".+\\\")?\\s+then\\s+goto\\s+(\\d+)",
			stackRegex = "(@?[#|~]\\d+)\\s*=\\s*(@?[#~]\\d+|\\d+|\\d+\\.\\d+|\\\".*\\\")",
			gotoRegex = "goto\\s+(\\d+)",
			callRegex = "&(\\d+)\\(\\)",
			moverRegex = "mover\\(\\)",
			rotarRegex = "rotar\\(\\)",
			dispararRegex = "disparar\\(\\)",
			recargarRegex = "recargar\\(\\)",
			escudoRegex = "escudo\\(\\)";

	public void updateMovimiento(int n) {
		this.var[4] = n;
	}

	public void updateVida(int n) {
		this.var[1] = n;
	}

	public Program(String name) {
		setName(name);
		this.globalVars = null;
		this.procs = new ArrayList<Proc>();
		this.globalVars = null;
	}

	public String getName() {
		return name;
	}

	private void setName(String name) {
		this.name = name;
	}

	public void setVars(String vars) {
		this.globalVars = vars;
	}

	public String getVars() {
		return this.globalVars;
	}

	public void addVar(String id) {
		if (globalVars == null)
			this.globalVars = id;
		else
			this.globalVars += "-" + id;
	}

	public void addFunction(String id, ArrayList<String> code, int temps) {
		procs.add(new Proc(id, temps, code));
	}

	public void configure(int stack_size, Robot robo, Game logger,
			int movimientos) {
		stack = new Object[stack_size];
		var = new Object[globalVars.split("-").length];
		for (Proc p : procs) {
			p.var = var;
			p.stack = stack;
		}
		execute("__init__", true);
		this.updateMovimiento(movimientos);
		this.robo = robo;
		this.logger = logger;
		this.var[1] = robo.getVida();
		this.var[2] = robo.getPoder();
		this.var[3] = robo.getRegeneracion();
	}

	public int execute(String method, boolean init) {

		Proc main = null;

		for (Proc p : procs)
			if (p.getId().equals(method)) {
				main = p;
				break;
			}

		p_if = Pattern.compile(ifRegex);
		p_expr = Pattern.compile(exprRegex);
		p_call = Pattern.compile(callRegex);
		p_goto = Pattern.compile(gotoRegex);
		p_stack = Pattern.compile(stackRegex);
		p_mover = Pattern.compile(moverRegex);
		p_rotar = Pattern.compile(rotarRegex);
		p_recargar = Pattern.compile(recargarRegex);
		p_disparar = Pattern.compile(dispararRegex);
		p_escudo = Pattern.compile(escudoRegex);

		for (int i = 0; i < main.getCode().length; i++) {
			Integer result = (Integer) this.compileAndExecute(main,
					main.getCode()[i]);

			if (destroy)
				break;

			if (result != null && result >= 0) {
				i = result - 1;
				continue;
			}
		}
		return -1;
	}

	public int execute(Proc method, boolean init) {
		p_if = Pattern.compile(ifRegex);
		p_expr = Pattern.compile(exprRegex);
		p_call = Pattern.compile(callRegex);
		p_goto = Pattern.compile(gotoRegex);
		p_stack = Pattern.compile(stackRegex);
		p_mover = Pattern.compile(moverRegex);
		p_rotar = Pattern.compile(rotarRegex);
		p_recargar = Pattern.compile(recargarRegex);
		p_disparar = Pattern.compile(dispararRegex);
		p_escudo = Pattern.compile(escudoRegex);

		for (int i = 0; i < method.getCode().length; i++) {
			Integer result = (Integer) this.compileAndExecute(method,
					method.getCode()[i]);
			if (destroy)
				break;

			if (result != null && result >= 0) {
				i = result - 1;
				continue;
			}
		}
		return -1;
	}

	private Object compileAndExecute(Proc ambiente, String codeLine) {
		try {
			Matcher currLine = p_mover.matcher(codeLine);
			if (currLine.matches()) {
				Integer p = (Integer) var[0];
				Integer dir = (Integer) stack[p];
				Integer n = (Integer) stack[p + 1];
				logger.mover(this, robo, dir, n);
				return null;
			}

			currLine = p_rotar.matcher(codeLine);
			if (currLine.matches()) {
				Integer p = (Integer) var[0];
				Integer dir = (Integer) stack[p];
				Integer n = (Integer) stack[p + 1];
				logger.girar(this, robo, 90 * dir, n);
				return null;
			}

			currLine = p_recargar.matcher(codeLine);
			if (currLine.matches()) {
				logger.recargar(this, robo);
				return null;
			}

			currLine = p_disparar.matcher(codeLine);
			if (currLine.matches()) {
				Integer p = (Integer) var[0];
				Integer municiones = (Integer) stack[p];
				logger.disparar(this, robo, municiones);
				return null;
			}

			currLine = p_escudo.matcher(codeLine);
			if (currLine.matches()) {
				Integer p = (Integer) var[0];
				Integer municiones = (Integer) stack[p];
				logger.escudo(this, robo, municiones);
				return null;
			}

			currLine = p_expr.matcher(codeLine);
			if (currLine.matches())
				return ambiente.calcExpr(currLine) == null ? null : -1;

			currLine = p_stack.matcher(codeLine);
			if (currLine.matches())
				return ambiente.inStack(currLine) == null ? null : -1;

			currLine = p_if.matcher(codeLine);
			if (currLine.matches()) {
				Integer result = (Integer) ambiente.checkExpr(currLine);
				return (result == null || result < 0) ? null : result;
			}

			currLine = p_goto.matcher(codeLine);
			if (currLine.matches())
				return ambiente.jump(currLine);

			currLine = p_call.matcher(codeLine);
			if (currLine.matches())
				return execute(
						procs.get((Integer) ambiente.callFunction(currLine)),
						false);

			return null;
		} catch (Exception e) {
			// TODO: handle exception
			System.err
					.println("**NullPointerException: Error en tiempo de ejecucion. Uno o mas valores no han sido asignados.");
			return null;
		}

	}

	public Proc getProc(String id) {
		for (Proc p : procs)
			if (p.getId().equals(id))
				return p;

		return null;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String method = "\t\t\t--- " + name + "  ---\n";

		for (String var : this.globalVars.split("-"))
			method += "$var " + var + ";\n";

		Pattern temp = Pattern.compile("#(\\d+)");
		Pattern var = Pattern.compile("~(\\d+)");
		Pattern stack = Pattern.compile("@(\\w+)");
		Pattern call = Pattern.compile("&(\\d+)\\(\\)");

		for (int j = 0; j < procs.size(); j++) {
			Proc proc = procs.get(j);

			// TODO Auto-generated method stub
			method += "\n\t$" + proc.getId() + "() : \n";
			for (int i = 0; i < proc.getCode().length; i++) {
				String line = proc.getCode()[i];
				method += "\t" + i + "\t";

				Matcher m = temp.matcher(line);

				if (m.find())
					line = line.replaceAll("#(\\d+)", "_t$1");

				m = var.matcher(line);
				while (m.find())
					line = m.replaceAll(globalVars.split("-")[Integer
							.parseInt(m.group(1))]);

				m = call.matcher(line);
				if (m.find())
					line = m.replaceAll(procs.get(Integer.parseInt(m.group(1)))
							.getId() + "()");

				m = stack.matcher(line);
				if (m.find())
					line = m.replaceAll("_stack[" + m.group(1) + "]");

				method += line + "\n";

			}
			method += "\n";
		}
		return method;
	}

	public String getCode() {
		String line = "----" + globalVars + "----\n";
		for (Proc p : procs) {
			line += "---" + p.getId() + "---\n";
			for (String i : p.getCode())
				line += i + "\n";
		}
		return line;
	}

	public ArrayList<Proc> getProcs() {
		return this.procs;
	}

	public String report() {
		String report = name + "@stack : " + Arrays.toString(stack) + "\n";
		report += name + "@vars : " + Arrays.toString(var) + "\n";

		for (Proc p : procs)
			report += name + "@" + p.toString() + "\n";

		return report;
	}
}
