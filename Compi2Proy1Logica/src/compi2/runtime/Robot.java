/**
 * 
 */
package compi2.runtime;

import java.io.Serializable;

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
public class Robot implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8788376834398048576L;
	private int vida, regeneracion, poder, movimiento, x, y, angle, escudo, id;
	private String nombre, ia_origen, ia_metodo, fecha;

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format(
				"Nombre:%s Vida:%d Poder:%d Regeneracion:%d Movimientos:%d IA:%s IA_Metodo: %s",
				nombre, vida, poder, regeneracion, movimiento, ia_origen, ia_metodo);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEscudo() {
		return escudo;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public void setEscudo(int escudo) {
		this.escudo = escudo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIa_origen() {
		return ia_origen;
	}

	public void setIa_origen(String ia_origen) {
		this.ia_origen = ia_origen;
	}

	public String getIa_metodo() {
		return ia_metodo;
	}

	public void setIa_metodo(String ia_metodo) {
		this.ia_metodo = ia_metodo;
	}

	public int getAngle() {
		return angle;
	}

	public void setAngle(int angle) {
		this.angle = angle;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getVida() {
		return vida;
	}

	public void setVida(int vida) {
		this.vida = vida;
	}

	public int getRegeneracion() {
		return regeneracion;
	}

	public void setRegeneracion(int regeneracion) {
		this.regeneracion = regeneracion;
	}

	public int getPoder() {
		return poder;
	}

	public void setPoder(int poder) {
		this.poder = poder;
	}

	public int getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(int movimiento) {
		this.movimiento = movimiento;
	}

	public static Robot unpackRobot(String robo) {
		Robot r = new Robot();
		String[] attrs = robo.split("@");
		r.setVida(Integer.parseInt(attrs[0]));
		r.setRegeneracion(Integer.parseInt(attrs[1]));
		r.setPoder(Integer.parseInt(attrs[2]));
		r.setMovimiento(Integer.parseInt(attrs[3]));
		r.setEscudo(Integer.parseInt(attrs[4]));
		r.setNombre(attrs[5]);
		r.setIa_origen(attrs[6]);
		r.setIa_metodo(attrs[7]);

		return r;
	}

	public String packRobot() {
		return this.getVida() + "@" + this.getRegeneracion() + "@"
				+ this.getPoder() + "@" + this.getMovimiento() + "@"
				+ this.getEscudo() + "@" + this.getNombre() + "@"
				+ this.getIa_origen() + "@" + this.getIa_metodo();
	}

}
