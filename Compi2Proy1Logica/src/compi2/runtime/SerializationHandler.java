/**
 * 
 */
package compi2.runtime;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * @author julian
 * 
 */
public class SerializationHandler {
	public static Object cargarPrograma(String ruta) {
		try {
			FileInputStream fis = new FileInputStream(ruta);

			ObjectInputStream ois = new ObjectInputStream(fis);
			Object obj = ois.readObject();
			ois.close();

			return obj;
		}

		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Object cargarPrograma(InputStream binario) {
		try {
			ObjectInputStream ois = new ObjectInputStream(binario);
			Object obj = ois.readObject();
			ois.close();

			return obj;
		}

		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static int guardarPrograma(String ruta, Object obj) {
		try {
			File path = new File(ruta);
			if (!path.exists())
				new File(path.getParent()).mkdirs();

			ObjectOutputStream fs1 = new ObjectOutputStream(
					new FileOutputStream(ruta));
			fs1.writeObject(obj);
			fs1.flush();
			fs1.close();

			return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public static int enviarObj(OutputStream outputChannel, Object obj) {
		try {
			ObjectOutputStream fs1 = new ObjectOutputStream(outputChannel);
			fs1.writeObject(obj + "\0");
			fs1.flush();
			fs1.close();

			return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public static Object recibirObj(InputStream inputChannel) {
		try {
			ObjectInputStream ois = new ObjectInputStream(inputChannel);
			Object obj = ois.readObject();
			ois.close();

			return obj;
		}

		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
