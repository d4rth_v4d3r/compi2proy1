package compi2.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import compi2.proy1.parsing.SocketParser.Item;

public class ItemList extends JDialog {

	public interface EventCallback {
		public void ok_do_action(Object selectValue);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5054941791538174212L;
	private final JPanel contentPanel = new JPanel();
	private final JList<Item> list;
	private final DefaultListModel<Item> model;
	private JButton okButton, cancelButton;

	/**
	 * Create the dialog.
	 */
	public ItemList(final EventCallback evt, String listName, JFrame parent,
			boolean modal) {
		super(parent, modal);
		this.setTitle(listName);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			model = new DefaultListModel<Item>();
			list = new JList<Item>(model);
			contentPanel.add(list, BorderLayout.CENTER);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				buttonPane.add(okButton);
				okButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						evt.ok_do_action(getSelectedItem());
						dispose();
						setVisible(false);
					}
				});
			}
			{
				cancelButton = new JButton("Cancelar");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						dispose();
						setVisible(false);
					}
				});
			}
		}
	}

	private Item getSelectedItem() {
		return list.getSelectedValue();
	}

	public void setListModel(Iterable<Item> list) {
		for (Item item : list)
			addItem(item);

		this.list.setSelectedIndex(0);
	}

	private void addItem(Item i) {
		this.model.addElement(i);
	}
}
