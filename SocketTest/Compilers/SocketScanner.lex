package compi2.proy1.parsing;

import java_cup.runtime.*;
import java.io.IOException;

%%

%class SocketScanner

%init{
		
%init}

%eofval{	
		return sym(SocketParsingSyms.EOF);
%eofval}

%public
%cup
%unicode
%ignorecase
%line  	//definimos el uso de contador de lineas
%char  	//definimos el uso de contador de caracteres
%column

%{
	StringBuffer str = new StringBuffer();
	boolean flag = false;
	
	private Symbol sym(int type)
	{
		return sym(type, yytext());
	}

	private Symbol sym(int type, Object value)
	{
		return new Symbol(type, yyline+1, yycolumn+1, value);
	}

%}

NUM 		= 	[0-9]+
string		=	\"[^\"]*\"

%state ANY, ANYTOK

%%
<YYINITIAL>{NUM}				{	return sym(SocketParsingSyms.num, new Integer(yytext()));	}
<YYINITIAL>{string}				{	return sym(SocketParsingSyms.string, new String(yytext()));	}
<YYINITIAL>"<"					{	flag = true; return sym(SocketParsingSyms.menor);	}
<YYINITIAL>">"					{	return sym(SocketParsingSyms.mayor);	}
<ANY>">"						{	str.setLength(0); yybegin(ANYTOK); return sym(SocketParsingSyms.mayor);	}
<YYINITIAL>"/"					{	flag = false; return sym(SocketParsingSyms.div);	}
<YYINITIAL>"codigo"				{	return sym(SocketParsingSyms.codigo);	}
<YYINITIAL>"="					{	return sym(SocketParsingSyms.igual);	}
<YYINITIAL>"nombre"				{	if(flag) yybegin(ANY); return sym(SocketParsingSyms.nombre);	}
<YYINITIAL>"movimiento"			{	return sym(SocketParsingSyms.movimiento);	}
<YYINITIAL>"regeneracion"		{	return sym(SocketParsingSyms.regeneracion);	}
<YYINITIAL>"escudo"				{	return sym(SocketParsingSyms.escudo);	}
<YYINITIAL>"inteligencia"		{	return sym(SocketParsingSyms.inteligencia);	}
<YYINITIAL>"metodo"				{	return sym(SocketParsingSyms.metodo);	}
<YYINITIAL>"origen"				{	return sym(SocketParsingSyms.origen);	}
<YYINITIAL>"configuracion"		{	return sym(SocketParsingSyms.configuracion);	}
<YYINITIAL>"default"			{	return sym(SocketParsingSyms.default_);	}
<YYINITIAL>"vida"				{	return sym(SocketParsingSyms.vida);	}
<YYINITIAL>"poder"				{	return sym(SocketParsingSyms.poder);	}
<YYINITIAL>"robot"				{	return sym(SocketParsingSyms.robot);	}
<YYINITIAL>"respuesta"			{	return sym(SocketParsingSyms.respuesta);	}
<YYINITIAL>"mensaje"			{	if(flag) yybegin(ANY); return sym(SocketParsingSyms.mensaje);	}
<YYINITIAL>"total"				{	return sym(SocketParsingSyms.total);	}
<YYINITIAL>"robots"				{	return sym(SocketParsingSyms.robots);	}
<YYINITIAL>"escenarios"			{	return sym(SocketParsingSyms.escenarios);	}


<YYINITIAL>","					{ 	return sym(SocketParsingSyms.coma); }
<YYINITIAL>"["					{ 	return sym(SocketParsingSyms.lbracket); }
<YYINITIAL>"]"					{ 	return sym(SocketParsingSyms.rbracket); }
<YYINITIAL>"-"					{ 	return sym(SocketParsingSyms.guion); }
<YYINITIAL>"objetos"			{ 	return sym(SocketParsingSyms.objetos); }
<YYINITIAL>"obj"				{ 	return sym(SocketParsingSyms.obj); }
<YYINITIAL>"fila"				{ 	return sym(SocketParsingSyms.fila); }
<YYINITIAL>"columna"			{ 	return sym(SocketParsingSyms.columna); }
<YYINITIAL>"tipo"				{ 	return sym(SocketParsingSyms.tipo); }
<YYINITIAL>"dimension"			{ 	return sym(SocketParsingSyms.dimension); }
<YYINITIAL>"descripcion" 		{ 	if(flag) yybegin(ANY); return sym(SocketParsingSyms.descripcion); }
<YYINITIAL>"escenario"			{ 	return sym(SocketParsingSyms.escenario); }
<YYINITIAL>"true"				{ 	return sym(SocketParsingSyms.bool, new Boolean(true)); }
<YYINITIAL>"false"				{ 	return sym(SocketParsingSyms.bool, new Boolean(false)); }
<YYINITIAL>"id"					{	return sym(SocketParsingSyms.idetq, new String(yytext())); }
<YYINITIAL>"fechacreacion"		{	if(flag) yybegin(ANY); return sym(SocketParsingSyms.fecha, new String(yytext())); }

<ANYTOK>[^"<"]+					{	yybegin(YYINITIAL); return sym(SocketParsingSyms.LID, yytext().trim()); }

<ANY, YYINITIAL>[\n\r\f]+		{   }
<ANY, YYINITIAL>[" "\t]+		{	}

<YYINITIAL>.					{	System.err.println("Token no reconocido en la respuesta: " + yytext()); }