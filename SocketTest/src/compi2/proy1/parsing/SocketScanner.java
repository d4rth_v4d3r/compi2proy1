/* The following code was generated by JFlex 1.4.3 on 23/09/13 11:34 PM */

package compi2.proy1.parsing;

import java_cup.runtime.*;
import java.io.IOException;


/**
 * This class is a scanner generated by 
 * <a href="http://www.jflex.de/">JFlex</a> 1.4.3
 * on 23/09/13 11:34 PM from the specification file
 * <tt>C:/Users/julia_000/Documents/eclipse_se_workspace/SocketTest/Compilers/SocketScanner.lex</tt>
 */
public class SocketScanner implements java_cup.runtime.Scanner {

  /** This character denotes the end of file */
  public static final int YYEOF = -1;

  /** initial size of the lookahead buffer */
  private static final int ZZ_BUFFERSIZE = 16384;

  /** lexical states */
  public static final int ANYTOK = 4;
  public static final int ANY = 2;
  public static final int YYINITIAL = 0;

  /**
   * ZZ_LEXSTATE[l] is the state in the DFA for the lexical state l
   * ZZ_LEXSTATE[l+1] is the state in the DFA for the lexical state l
   *                  at the beginning of a line
   * l is of the form l = 2*k, k a non negative integer
   */
  private static final int ZZ_LEXSTATE[] = { 
     0,  0,  1,  1,  2, 2
  };

  /** 
   * Translates characters to character classes
   */
  private static final String ZZ_CMAP_PACKED = 
    "\11\0\1\40\1\37\1\0\2\37\22\0\1\40\1\0\1\2\11\0"+
    "\1\32\1\35\1\0\1\5\12\1\2\0\1\3\1\13\1\4\2\0"+
    "\1\23\1\16\1\6\1\10\1\20\1\27\1\12\1\36\1\11\1\31"+
    "\1\0\1\26\1\15\1\14\1\7\1\30\1\0\1\17\1\24\1\22"+
    "\1\25\1\21\4\0\1\33\1\0\1\34\3\0\1\23\1\16\1\6"+
    "\1\10\1\20\1\27\1\12\1\36\1\11\1\31\1\0\1\26\1\15"+
    "\1\14\1\7\1\30\1\0\1\17\1\24\1\22\1\25\1\21\uff89\0";

  /** 
   * Translates characters to character classes
   */
  private static final char [] ZZ_CMAP = zzUnpackCMap(ZZ_CMAP_PACKED);

  /** 
   * Translates DFA states to action switch labels.
   */
  private static final int [] ZZ_ACTION = zzUnpackAction();

  private static final String ZZ_ACTION_PACKED_0 =
    "\3\0\1\1\1\2\1\1\1\3\1\4\1\5\4\1"+
    "\1\6\10\1\1\7\1\10\1\11\1\12\2\13\1\14"+
    "\1\15\1\0\1\16\5\0\1\17\22\0\1\20\47\0"+
    "\1\21\1\0\1\22\1\23\1\24\20\0\1\25\4\0"+
    "\1\26\1\0\1\27\1\30\1\31\3\0\1\32\4\0"+
    "\1\33\2\0\1\34\1\35\3\0\1\36\2\0\1\37"+
    "\1\40\2\0\1\41\2\0\1\42\16\0\1\43\4\0"+
    "\1\44\1\45\4\0\1\46\1\0\1\47\2\0\1\50"+
    "\4\0\1\51\1\52\1\0\1\53\1\54";

  private static int [] zzUnpackAction() {
    int [] result = new int[194];
    int offset = 0;
    offset = zzUnpackAction(ZZ_ACTION_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAction(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /** 
   * Translates a state to a row index in the transition table
   */
  private static final int [] ZZ_ROWMAP = zzUnpackRowMap();

  private static final String ZZ_ROWMAP_PACKED_0 =
    "\0\0\0\41\0\102\0\143\0\204\0\245\0\143\0\143"+
    "\0\143\0\306\0\347\0\u0108\0\u0129\0\143\0\u014a\0\u016b"+
    "\0\u018c\0\u01ad\0\u01ce\0\u01ef\0\u0210\0\u0231\0\143\0\143"+
    "\0\143\0\143\0\u0252\0\u0273\0\143\0\102\0\245\0\143"+
    "\0\u0294\0\u02b5\0\u02d6\0\u02f7\0\u0318\0\143\0\u0339\0\u035a"+
    "\0\u037b\0\u039c\0\u03bd\0\u03de\0\u03ff\0\u0420\0\u0441\0\u0462"+
    "\0\u0483\0\u04a4\0\u04c5\0\u04e6\0\u0507\0\u0528\0\u0549\0\u056a"+
    "\0\u058b\0\u05ac\0\u05cd\0\u05ee\0\u060f\0\u0630\0\u0651\0\u0672"+
    "\0\u0693\0\u06b4\0\u06d5\0\u06f6\0\u0717\0\u0738\0\u0759\0\u077a"+
    "\0\u079b\0\u07bc\0\u07dd\0\u07fe\0\u081f\0\u0840\0\u0861\0\u0882"+
    "\0\u08a3\0\u08c4\0\u08e5\0\u0906\0\u0927\0\u0948\0\u0969\0\u098a"+
    "\0\u09ab\0\u09cc\0\u09ed\0\u0a0e\0\u0a2f\0\u0a50\0\u0a71\0\u0a92"+
    "\0\143\0\u0ab3\0\143\0\143\0\143\0\u0ad4\0\u0af5\0\u0b16"+
    "\0\u0b37\0\u0b58\0\u0b79\0\u0b9a\0\u0bbb\0\u0bdc\0\u0bfd\0\u0c1e"+
    "\0\u0c3f\0\u0c60\0\u0c81\0\u0ca2\0\u0cc3\0\u0ce4\0\u0d05\0\u0d26"+
    "\0\u0d47\0\u0d68\0\143\0\u0d89\0\143\0\143\0\143\0\u0daa"+
    "\0\u0dcb\0\u0dec\0\143\0\u0e0d\0\u0e2e\0\u0e4f\0\u0e70\0\143"+
    "\0\u0e91\0\u0eb2\0\143\0\143\0\u0ed3\0\u0ef4\0\u0f15\0\143"+
    "\0\u0f36\0\u0f57\0\143\0\143\0\u0f78\0\u0f99\0\143\0\u0fba"+
    "\0\u0fdb\0\143\0\u0ffc\0\u101d\0\u103e\0\u105f\0\u1080\0\u10a1"+
    "\0\u10c2\0\u10e3\0\u1104\0\u1125\0\u1146\0\u1167\0\u1188\0\u11a9"+
    "\0\143\0\u11ca\0\u11eb\0\u120c\0\u122d\0\143\0\u124e\0\u126f"+
    "\0\u1290\0\u12b1\0\u12d2\0\143\0\u12f3\0\143\0\u1314\0\u1335"+
    "\0\143\0\u1356\0\u1377\0\u1398\0\u13b9\0\143\0\143\0\u13da"+
    "\0\143\0\143";

  private static int [] zzUnpackRowMap() {
    int [] result = new int[194];
    int offset = 0;
    offset = zzUnpackRowMap(ZZ_ROWMAP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackRowMap(String packed, int offset, int [] result) {
    int i = 0;  /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int high = packed.charAt(i++) << 16;
      result[j++] = high | packed.charAt(i++);
    }
    return j;
  }

  /** 
   * The transition table of the DFA
   */
  private static final int [] ZZ_TRANS = zzUnpackTrans();

  private static final String ZZ_TRANS_PACKED_0 =
    "\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13"+
    "\1\14\1\15\1\4\1\16\1\17\1\20\1\4\1\21"+
    "\1\22\1\23\1\24\4\4\1\25\1\26\1\4\1\27"+
    "\1\30\1\31\1\32\1\4\1\33\1\34\4\0\1\35"+
    "\32\0\1\33\1\34\3\36\1\0\35\36\42\0\1\5"+
    "\37\0\2\37\1\40\36\37\7\0\1\41\47\0\1\42"+
    "\1\43\32\0\1\44\6\0\1\45\30\0\1\46\3\0"+
    "\1\47\33\0\1\50\40\0\1\51\10\0\1\52\27\0"+
    "\1\53\10\0\1\54\44\0\1\55\25\0\1\56\36\0"+
    "\1\57\1\0\1\60\5\0\1\61\32\0\1\62\6\0"+
    "\1\63\2\0\1\64\24\0\1\65\70\0\1\33\41\0"+
    "\1\34\10\0\1\66\3\0\1\67\11\0\1\70\43\0"+
    "\1\71\20\0\1\72\44\0\1\73\47\0\1\74\2\0"+
    "\1\75\33\0\1\76\33\0\1\77\44\0\1\100\33\0"+
    "\1\101\5\0\1\102\34\0\1\103\34\0\1\104\11\0"+
    "\1\105\22\0\1\106\42\0\1\107\52\0\1\110\46\0"+
    "\1\111\35\0\1\112\41\0\1\113\20\0\1\114\60\0"+
    "\1\115\22\0\1\116\41\0\1\117\56\0\1\120\36\0"+
    "\1\121\33\0\1\122\32\0\1\123\46\0\1\124\26\0"+
    "\1\125\55\0\1\126\35\0\1\127\36\0\1\130\33\0"+
    "\1\131\53\0\1\132\23\0\1\133\40\0\1\134\51\0"+
    "\1\135\50\0\1\136\30\0\1\137\4\0\1\140\36\0"+
    "\1\141\40\0\1\142\24\0\1\143\51\0\1\144\43\0"+
    "\1\145\53\0\1\146\26\0\1\147\34\0\1\150\32\0"+
    "\1\151\37\0\1\152\44\0\1\153\45\0\1\154\36\0"+
    "\1\155\34\0\1\156\43\0\1\157\46\0\1\160\41\0"+
    "\1\161\31\0\1\162\36\0\1\163\46\0\1\164\25\0"+
    "\1\165\52\0\1\166\32\0\1\167\51\0\1\170\27\0"+
    "\1\171\34\0\1\172\56\0\1\173\35\0\1\174\35\0"+
    "\1\175\37\0\1\176\30\0\1\177\43\0\1\200\42\0"+
    "\1\201\33\0\1\202\45\0\1\203\50\0\1\204\25\0"+
    "\1\205\55\0\1\206\23\0\1\207\47\0\1\210\31\0"+
    "\1\211\60\0\1\212\16\0\1\213\55\0\1\214\34\0"+
    "\1\215\40\0\1\216\43\0\1\217\24\0\1\220\37\0"+
    "\1\221\57\0\1\222\36\0\1\223\41\0\1\224\25\0"+
    "\1\225\57\0\1\226\32\0\1\227\30\0\1\230\46\0"+
    "\1\231\40\0\1\232\37\0\1\233\45\0\1\234\33\0"+
    "\1\235\40\0\1\236\40\0\1\237\30\0\1\240\37\0"+
    "\1\241\52\0\1\242\34\0\1\243\47\0\1\244\37\0"+
    "\1\245\27\0\1\246\47\0\1\247\43\0\1\250\31\0"+
    "\1\251\35\0\1\252\43\0\1\253\46\0\1\254\24\0"+
    "\1\255\55\0\1\256\24\0\1\257\54\0\1\260\23\0"+
    "\1\261\41\0\1\262\37\0\1\263\41\0\1\264\42\0"+
    "\1\265\53\0\1\266\22\0\1\267\43\0\1\270\43\0"+
    "\1\271\35\0\1\272\36\0\1\273\42\0\1\274\36\0"+
    "\1\275\54\0\1\276\31\0\1\277\33\0\1\300\45\0"+
    "\1\301\40\0\1\302\24\0";

  private static int [] zzUnpackTrans() {
    int [] result = new int[5115];
    int offset = 0;
    offset = zzUnpackTrans(ZZ_TRANS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackTrans(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      value--;
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /* error codes */
  private static final int ZZ_UNKNOWN_ERROR = 0;
  private static final int ZZ_NO_MATCH = 1;
  private static final int ZZ_PUSHBACK_2BIG = 2;

  /* error messages for the codes above */
  private static final String ZZ_ERROR_MSG[] = {
    "Unkown internal scanner error",
    "Error: could not match input",
    "Error: pushback value was too large"
  };

  /**
   * ZZ_ATTRIBUTE[aState] contains the attributes of state <code>aState</code>
   */
  private static final int [] ZZ_ATTRIBUTE = zzUnpackAttribute();

  private static final String ZZ_ATTRIBUTE_PACKED_0 =
    "\3\0\1\11\2\1\3\11\4\1\1\11\10\1\4\11"+
    "\2\1\1\11\1\1\1\0\1\11\5\0\1\11\22\0"+
    "\1\1\47\0\1\11\1\0\3\11\20\0\1\1\4\0"+
    "\1\11\1\0\3\11\3\0\1\11\4\0\1\11\2\0"+
    "\2\11\3\0\1\11\2\0\2\11\2\0\1\11\2\0"+
    "\1\11\16\0\1\11\4\0\1\11\1\1\4\0\1\11"+
    "\1\0\1\11\2\0\1\11\4\0\2\11\1\0\2\11";

  private static int [] zzUnpackAttribute() {
    int [] result = new int[194];
    int offset = 0;
    offset = zzUnpackAttribute(ZZ_ATTRIBUTE_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAttribute(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /** the input device */
  private java.io.Reader zzReader;

  /** the current state of the DFA */
  private int zzState;

  /** the current lexical state */
  private int zzLexicalState = YYINITIAL;

  /** this buffer contains the current text to be matched and is
      the source of the yytext() string */
  private char zzBuffer[] = new char[ZZ_BUFFERSIZE];

  /** the textposition at the last accepting state */
  private int zzMarkedPos;

  /** the current text position in the buffer */
  private int zzCurrentPos;

  /** startRead marks the beginning of the yytext() string in the buffer */
  private int zzStartRead;

  /** endRead marks the last character in the buffer, that has been read
      from input */
  private int zzEndRead;

  /** number of newlines encountered up to the start of the matched text */
  private int yyline;

  /** the number of characters up to the start of the matched text */
  private int yychar;

  /**
   * the number of characters from the last newline up to the start of the 
   * matched text
   */
  private int yycolumn;

  /** 
   * zzAtBOL == true <=> the scanner is currently at the beginning of a line
   */
  private boolean zzAtBOL = true;

  /** zzAtEOF == true <=> the scanner is at the EOF */
  private boolean zzAtEOF;

  /** denotes if the user-EOF-code has already been executed */
  private boolean zzEOFDone;

  /* user code: */
	StringBuffer str = new StringBuffer();
	boolean flag = false;
	
	private Symbol sym(int type)
	{
		return sym(type, yytext());
	}

	private Symbol sym(int type, Object value)
	{
		return new Symbol(type, yyline+1, yycolumn+1, value);
	}



  /**
   * Creates a new scanner
   * There is also a java.io.InputStream version of this constructor.
   *
   * @param   in  the java.io.Reader to read input from.
   */
  public SocketScanner(java.io.Reader in) {
  		
    this.zzReader = in;
  }

  /**
   * Creates a new scanner.
   * There is also java.io.Reader version of this constructor.
   *
   * @param   in  the java.io.Inputstream to read input from.
   */
  public SocketScanner(java.io.InputStream in) {
    this(new java.io.InputStreamReader(in));
  }

  /** 
   * Unpacks the compressed character translation table.
   *
   * @param packed   the packed character translation table
   * @return         the unpacked character translation table
   */
  private static char [] zzUnpackCMap(String packed) {
    char [] map = new char[0x10000];
    int i = 0;  /* index in packed string  */
    int j = 0;  /* index in unpacked array */
    while (i < 140) {
      int  count = packed.charAt(i++);
      char value = packed.charAt(i++);
      do map[j++] = value; while (--count > 0);
    }
    return map;
  }


  /**
   * Refills the input buffer.
   *
   * @return      <code>false</code>, iff there was new input.
   * 
   * @exception   java.io.IOException  if any I/O-Error occurs
   */
  private boolean zzRefill() throws java.io.IOException {

    /* first: make room (if you can) */
    if (zzStartRead > 0) {
      System.arraycopy(zzBuffer, zzStartRead,
                       zzBuffer, 0,
                       zzEndRead-zzStartRead);

      /* translate stored positions */
      zzEndRead-= zzStartRead;
      zzCurrentPos-= zzStartRead;
      zzMarkedPos-= zzStartRead;
      zzStartRead = 0;
    }

    /* is the buffer big enough? */
    if (zzCurrentPos >= zzBuffer.length) {
      /* if not: blow it up */
      char newBuffer[] = new char[zzCurrentPos*2];
      System.arraycopy(zzBuffer, 0, newBuffer, 0, zzBuffer.length);
      zzBuffer = newBuffer;
    }

    /* finally: fill the buffer with new input */
    int numRead = zzReader.read(zzBuffer, zzEndRead,
                                            zzBuffer.length-zzEndRead);

    if (numRead > 0) {
      zzEndRead+= numRead;
      return false;
    }
    // unlikely but not impossible: read 0 characters, but not at end of stream    
    if (numRead == 0) {
      int c = zzReader.read();
      if (c == -1) {
        return true;
      } else {
        zzBuffer[zzEndRead++] = (char) c;
        return false;
      }     
    }

	// numRead < 0
    return true;
  }

    
  /**
   * Closes the input stream.
   */
  public final void yyclose() throws java.io.IOException {
    zzAtEOF = true;            /* indicate end of file */
    zzEndRead = zzStartRead;  /* invalidate buffer    */

    if (zzReader != null)
      zzReader.close();
  }


  /**
   * Resets the scanner to read from a new input stream.
   * Does not close the old reader.
   *
   * All internal variables are reset, the old input stream 
   * <b>cannot</b> be reused (internal buffer is discarded and lost).
   * Lexical state is set to <tt>ZZ_INITIAL</tt>.
   *
   * @param reader   the new input stream 
   */
  public final void yyreset(java.io.Reader reader) {
    zzReader = reader;
    zzAtBOL  = true;
    zzAtEOF  = false;
    zzEOFDone = false;
    zzEndRead = zzStartRead = 0;
    zzCurrentPos = zzMarkedPos = 0;
    yyline = yychar = yycolumn = 0;
    zzLexicalState = YYINITIAL;
  }


  /**
   * Returns the current lexical state.
   */
  public final int yystate() {
    return zzLexicalState;
  }


  /**
   * Enters a new lexical state
   *
   * @param newState the new lexical state
   */
  public final void yybegin(int newState) {
    zzLexicalState = newState;
  }


  /**
   * Returns the text matched by the current regular expression.
   */
  public final String yytext() {
    return new String( zzBuffer, zzStartRead, zzMarkedPos-zzStartRead );
  }


  /**
   * Returns the character at position <tt>pos</tt> from the 
   * matched text. 
   * 
   * It is equivalent to yytext().charAt(pos), but faster
   *
   * @param pos the position of the character to fetch. 
   *            A value from 0 to yylength()-1.
   *
   * @return the character at position pos
   */
  public final char yycharat(int pos) {
    return zzBuffer[zzStartRead+pos];
  }


  /**
   * Returns the length of the matched text region.
   */
  public final int yylength() {
    return zzMarkedPos-zzStartRead;
  }


  /**
   * Reports an error that occured while scanning.
   *
   * In a wellformed scanner (no or only correct usage of 
   * yypushback(int) and a match-all fallback rule) this method 
   * will only be called with things that "Can't Possibly Happen".
   * If this method is called, something is seriously wrong
   * (e.g. a JFlex bug producing a faulty scanner etc.).
   *
   * Usual syntax/scanner level error handling should be done
   * in error fallback rules.
   *
   * @param   errorCode  the code of the errormessage to display
   */
  private void zzScanError(int errorCode) {
    String message;
    try {
      message = ZZ_ERROR_MSG[errorCode];
    }
    catch (ArrayIndexOutOfBoundsException e) {
      message = ZZ_ERROR_MSG[ZZ_UNKNOWN_ERROR];
    }

    throw new Error(message);
  } 


  /**
   * Pushes the specified amount of characters back into the input stream.
   *
   * They will be read again by then next call of the scanning method
   *
   * @param number  the number of characters to be read again.
   *                This number must not be greater than yylength()!
   */
  public void yypushback(int number)  {
    if ( number > yylength() )
      zzScanError(ZZ_PUSHBACK_2BIG);

    zzMarkedPos -= number;
  }


  /**
   * Contains user EOF-code, which will be executed exactly once,
   * when the end of file is reached
   */
  private void zzDoEOF() throws java.io.IOException {
    if (!zzEOFDone) {
      zzEOFDone = true;
      yyclose();
    }
  }


  /**
   * Resumes scanning until the next regular expression is matched,
   * the end of input is encountered or an I/O-Error occurs.
   *
   * @return      the next token
   * @exception   java.io.IOException  if any I/O-Error occurs
   */
  public java_cup.runtime.Symbol next_token() throws java.io.IOException {
    int zzInput;
    int zzAction;

    // cached fields:
    int zzCurrentPosL;
    int zzMarkedPosL;
    int zzEndReadL = zzEndRead;
    char [] zzBufferL = zzBuffer;
    char [] zzCMapL = ZZ_CMAP;

    int [] zzTransL = ZZ_TRANS;
    int [] zzRowMapL = ZZ_ROWMAP;
    int [] zzAttrL = ZZ_ATTRIBUTE;

    while (true) {
      zzMarkedPosL = zzMarkedPos;

      yychar+= zzMarkedPosL-zzStartRead;

      boolean zzR = false;
      for (zzCurrentPosL = zzStartRead; zzCurrentPosL < zzMarkedPosL;
                                                             zzCurrentPosL++) {
        switch (zzBufferL[zzCurrentPosL]) {
        case '\u000B':
        case '\u000C':
        case '\u0085':
        case '\u2028':
        case '\u2029':
          yyline++;
          yycolumn = 0;
          zzR = false;
          break;
        case '\r':
          yyline++;
          yycolumn = 0;
          zzR = true;
          break;
        case '\n':
          if (zzR)
            zzR = false;
          else {
            yyline++;
            yycolumn = 0;
          }
          break;
        default:
          zzR = false;
          yycolumn++;
        }
      }

      if (zzR) {
        // peek one character ahead if it is \n (if we have counted one line too much)
        boolean zzPeek;
        if (zzMarkedPosL < zzEndReadL)
          zzPeek = zzBufferL[zzMarkedPosL] == '\n';
        else if (zzAtEOF)
          zzPeek = false;
        else {
          boolean eof = zzRefill();
          zzEndReadL = zzEndRead;
          zzMarkedPosL = zzMarkedPos;
          zzBufferL = zzBuffer;
          if (eof) 
            zzPeek = false;
          else 
            zzPeek = zzBufferL[zzMarkedPosL] == '\n';
        }
        if (zzPeek) yyline--;
      }
      zzAction = -1;

      zzCurrentPosL = zzCurrentPos = zzStartRead = zzMarkedPosL;
  
      zzState = ZZ_LEXSTATE[zzLexicalState];


      zzForAction: {
        while (true) {
    
          if (zzCurrentPosL < zzEndReadL)
            zzInput = zzBufferL[zzCurrentPosL++];
          else if (zzAtEOF) {
            zzInput = YYEOF;
            break zzForAction;
          }
          else {
            // store back cached positions
            zzCurrentPos  = zzCurrentPosL;
            zzMarkedPos   = zzMarkedPosL;
            boolean eof = zzRefill();
            // get translated positions and possibly new buffer
            zzCurrentPosL  = zzCurrentPos;
            zzMarkedPosL   = zzMarkedPos;
            zzBufferL      = zzBuffer;
            zzEndReadL     = zzEndRead;
            if (eof) {
              zzInput = YYEOF;
              break zzForAction;
            }
            else {
              zzInput = zzBufferL[zzCurrentPosL++];
            }
          }
          int zzNext = zzTransL[ zzRowMapL[zzState] + zzCMapL[zzInput] ];
          if (zzNext == -1) break zzForAction;
          zzState = zzNext;

          int zzAttributes = zzAttrL[zzState];
          if ( (zzAttributes & 1) == 1 ) {
            zzAction = zzState;
            zzMarkedPosL = zzCurrentPosL;
            if ( (zzAttributes & 8) == 8 ) break zzForAction;
          }

        }
      }

      // store back cached position
      zzMarkedPos = zzMarkedPosL;

      switch (zzAction < 0 ? zzAction : ZZ_ACTION[zzAction]) {
        case 18: 
          { return sym(SocketParsingSyms.tipo);
          }
        case 45: break;
        case 41: 
          { return sym(SocketParsingSyms.inteligencia);
          }
        case 46: break;
        case 12: 
          { str.setLength(0); yybegin(ANYTOK); return sym(SocketParsingSyms.mayor);
          }
        case 47: break;
        case 1: 
          { System.err.println("Token no reconocido en la respuesta: " + yytext());
          }
        case 48: break;
        case 39: 
          { return sym(SocketParsingSyms.escenarios);
          }
        case 49: break;
        case 24: 
          { return sym(SocketParsingSyms.poder);
          }
        case 50: break;
        case 36: 
          { return sym(SocketParsingSyms.respuesta);
          }
        case 51: break;
        case 16: 
          { return sym(SocketParsingSyms.obj);
          }
        case 52: break;
        case 4: 
          { return sym(SocketParsingSyms.mayor);
          }
        case 53: break;
        case 5: 
          { flag = false; return sym(SocketParsingSyms.div);
          }
        case 54: break;
        case 31: 
          { return sym(SocketParsingSyms.columna);
          }
        case 55: break;
        case 19: 
          { return sym(SocketParsingSyms.bool, new Boolean(true));
          }
        case 56: break;
        case 6: 
          { return sym(SocketParsingSyms.igual);
          }
        case 57: break;
        case 10: 
          { return sym(SocketParsingSyms.guion);
          }
        case 58: break;
        case 43: 
          { return sym(SocketParsingSyms.configuracion);
          }
        case 59: break;
        case 25: 
          { return sym(SocketParsingSyms.codigo);
          }
        case 60: break;
        case 42: 
          { return sym(SocketParsingSyms.regeneracion);
          }
        case 61: break;
        case 14: 
          { return sym(SocketParsingSyms.string, new String(yytext()));
          }
        case 62: break;
        case 13: 
          { yybegin(YYINITIAL); return sym(SocketParsingSyms.LID, yytext().trim());
          }
        case 63: break;
        case 21: 
          { return sym(SocketParsingSyms.robot);
          }
        case 64: break;
        case 30: 
          { return sym(SocketParsingSyms.escudo);
          }
        case 65: break;
        case 8: 
          { return sym(SocketParsingSyms.lbracket);
          }
        case 66: break;
        case 28: 
          { return sym(SocketParsingSyms.metodo);
          }
        case 67: break;
        case 32: 
          { return sym(SocketParsingSyms.objetos);
          }
        case 68: break;
        case 3: 
          { flag = true; return sym(SocketParsingSyms.menor);
          }
        case 69: break;
        case 27: 
          { if(flag) yybegin(ANY); return sym(SocketParsingSyms.nombre);
          }
        case 70: break;
        case 17: 
          { return sym(SocketParsingSyms.vida);
          }
        case 71: break;
        case 23: 
          { return sym(SocketParsingSyms.bool, new Boolean(false));
          }
        case 72: break;
        case 38: 
          { return sym(SocketParsingSyms.movimiento);
          }
        case 73: break;
        case 20: 
          { return sym(SocketParsingSyms.fila);
          }
        case 74: break;
        case 15: 
          { return sym(SocketParsingSyms.idetq, new String(yytext()));
          }
        case 75: break;
        case 37: 
          { return sym(SocketParsingSyms.escenario);
          }
        case 76: break;
        case 2: 
          { return sym(SocketParsingSyms.num, new Integer(yytext()));
          }
        case 77: break;
        case 40: 
          { if(flag) yybegin(ANY); return sym(SocketParsingSyms.descripcion);
          }
        case 78: break;
        case 26: 
          { return sym(SocketParsingSyms.origen);
          }
        case 79: break;
        case 34: 
          { if(flag) yybegin(ANY); return sym(SocketParsingSyms.mensaje);
          }
        case 80: break;
        case 29: 
          { return sym(SocketParsingSyms.robots);
          }
        case 81: break;
        case 22: 
          { return sym(SocketParsingSyms.total);
          }
        case 82: break;
        case 44: 
          { if(flag) yybegin(ANY); return sym(SocketParsingSyms.fecha, new String(yytext()));
          }
        case 83: break;
        case 35: 
          { return sym(SocketParsingSyms.dimension);
          }
        case 84: break;
        case 9: 
          { return sym(SocketParsingSyms.rbracket);
          }
        case 85: break;
        case 33: 
          { return sym(SocketParsingSyms.default_);
          }
        case 86: break;
        case 11: 
          { 
          }
        case 87: break;
        case 7: 
          { return sym(SocketParsingSyms.coma);
          }
        case 88: break;
        default: 
          if (zzInput == YYEOF && zzStartRead == zzCurrentPos) {
            zzAtEOF = true;
            zzDoEOF();
              { 		return sym(SocketParsingSyms.EOF);
 }
          } 
          else {
            zzScanError(ZZ_NO_MATCH);
          }
      }
    }
  }


}
