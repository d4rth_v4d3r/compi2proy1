/**
 * 
 */
package compi2.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import compi2.proy1.parsing.ServerParser;
import compi2.runtime.Escenario;
import compi2.runtime.Robot;

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
public class ApplicationServer {
	private ServerSocket appServerSocket;
	private boolean continuar;
	private SocketConnection dataServerSocket;
	public static final String connStarted = "<respuesta><codigo>61</codigo><mensaje>Ha iniciado la conexion.</mensaje></respuesta>",
			connFinished = "<respuesta><codigo>60</codigo><mensaje>Ha finalizado la conexion.</mensaje></respuesta>",
			err = "<respuesta><codigo>22</codigo><mensaje>Error interno.</mensaje></respuesta>";
	public static ArrayList<Sesion> sesiones;

	public class Sesion {
		private SocketConnection client;
		private String username;
		public ArrayList<Integer> robots = new ArrayList<Integer>();
		public ArrayList<Escenario> escenarios = new ArrayList<Escenario>();
		public Robot robot = null;
		public Escenario esc = null;

		public Sesion(String username, SocketConnection client) {
			setUsername(username);
			setClient(client);
		}

		public SocketConnection getClient() {
			return client;
		}

		public void setClient(SocketConnection client) {
			this.client = client;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return username;
		}

	}

	public ApplicationServer(int puerto) {
		try {
			this.appServerSocket = new ServerSocket(puerto);
			this.dataServerSocket = new SocketConnection("localhost", 9001);
			continuar = true;
			sesiones = new ArrayList<Sesion>();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("No se encontro el servidor: "
					+ this.appServerSocket.getInetAddress());
			System.exit(1);
		}
	}

	public void shutdown() {
		try {
			this.appServerSocket.close();
			this.dataServerSocket.close();
			continuar = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("No se pudo cerrar el servidor: "
					+ this.appServerSocket.getInetAddress());
			System.exit(1);
		}
	}

	public void listen() {
		Socket client = null;
		while (continuar) {
			try {
				client = appServerSocket.accept();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				System.err.println("No se recibieron conexiones en: "
						+ this.appServerSocket.getInetAddress());
			}
			if (client != null) {
				final SocketConnection conn = new SocketConnection(client);

				Thread tr = new Thread(new Runnable() {
					Sesion s;

					@Override
					public void run() {
						// TODO Auto-generated method stub
						conn.sendData(connStarted + "\0");
						listen: while (continuar) {
							if (!conn.isAlive())
								break;
							String dataRec = conn.readData();
							if (dataRec == null || dataRec.equals(connFinished))
								break;
							ServerParser parser = new ServerParser(
									new ByteArrayInputStream(dataRec.getBytes()));
							try {
								parser.parse();

								if (parser.codigo == 50) {
									System.out.println("Usuario " + parser.usr1
											+ " envia una solicitud a "
											+ parser.usr2);
									for (Sesion s1 : sesiones)
										if (s1.username.equals(parser.usr2)) {
											s1.client.sendData(String
													.format("<peticion><batalla><usuario1>%s</usuario1><escenario>%d</escenario></batalla></peticion>",
															parser.usr1,
															s.esc.getId()));
											String respuesta = s1.client
													.readData();
											ServerParser parser2 = new ServerParser(
													new ByteArrayInputStream(
															respuesta
																	.getBytes()));
											parser2.parse();
											conn.sendData(respuesta + "\0");
											if (parser2.status)
												System.gc();
											continue listen;
										}
								}
								if (parser.codigo == 7) {
									System.out.println("Usuario " + parser.msj
											+ " ha cerrado sesion.");
									for (int i = 0; i < sesiones.size(); ++i) {
										if (sesiones.get(i).getUsername()
												.equals(parser.msj))
											sesiones.remove(i);
									}
								}
								if (!parser.object1.toString().equals("")) {
									dataServerSocket.sendData(parser.object1
											.toString());
									ServerParser parser2 = new ServerParser(
											new ByteArrayInputStream(
													dataServerSocket.readData()
															.getBytes()));
									parser2.parse();
									if (parser2.codigo == 1) {
										System.out.println("Usuario "
												+ parser.msj
												+ " ha iniciado sesion.");
										s = new Sesion(parser.msj, conn);
										sesiones.add(s);
									} else if (parser2.codigo == 11) {
										s.robots = parser2.robots;
										s.escenarios = parser2.lesc;
										System.out
												.println("Robot y Escenarios: "
														+ s.robots
														+ s.escenarios);
									}
									if (parser2.codigo == 71) {
										s.robot = parser2.robot;
										for (Escenario e : s.escenarios)
											if (e.getId() == parser.row) {
												s.esc = e;
												break;
											}

										System.out.println("Robot y Esc:  "
												+ s.robot + s.esc);
									}
									conn.sendData(parser2.object.toString()
											+ "\0");
								} else
									conn.sendData(conn.getInfo() + "|"
											+ dataRec + "\0");
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								conn.sendData(err + "\0");
							}
						}
					}
				});
				tr.start();
			}
			try {
				Thread.sleep(2);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param args
	 * @throws Exception
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException, Exception {
		// TODO Auto-generated method stub
		final ApplicationServer ap = new ApplicationServer(9010);
		Thread tr = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				ap.listen();
			}
		});
		tr.start();
		ap.dataServerSocket.interactiveConsole();
		ap.continuar = false;
	}

}
